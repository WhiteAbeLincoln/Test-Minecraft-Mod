package com.abe.tutorial.help;

import java.util.Random;

import com.abe.tutorial.TutorialMod;
import com.abe.tutorial.crafting.MashingRecipes;

import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityList;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;

public class RegisterHelper {
	public static void registerBlock(Block block){
		GameRegistry.registerBlock(block, Reference.MODID + "_" + block.getUnlocalizedName().substring(5));
	}
	
	/**
     * Register a block with the world, with the specified item class and block name
     * @param block The block to register
     * @param itemClass The item type to register with it : null registers a block without associated item.
     */
	public static void registerBlock(Block block, Class itemClass){
		GameRegistry.registerBlock(block, itemClass, Reference.MODID + "_" + block.getUnlocalizedName().substring(5));
	}
	
	public static void registerItem(Item item){
		GameRegistry.registerItem(item, Reference.MODID + "_" + item.getUnlocalizedName().substring(5));
	}
	
	public static void registerTileEntity(Class tileEntityClass){
		GameRegistry.registerTileEntity(tileEntityClass, Reference.MODID + "_" + tileEntityClass.getCanonicalName());
	}
	
	public static void registerEntity(Class entityClass, String name){
		
		int entityID = EntityRegistry.findGlobalUniqueEntityId();
		long seed = name.hashCode();
		Random rand = new Random(seed);
		int primaryColor = rand.nextInt() * 16777215;
		int secondaryColor = rand.nextInt() * 16777215;
	
		EntityRegistry.registerGlobalEntityID(entityClass, name, entityID);
		EntityRegistry.registerModEntity(entityClass, name, entityID, TutorialMod.instance, 64, 1, true);
		EntityList.entityEggs.put(Integer.valueOf(entityID), new EntityList.EntityEggInfo(entityID, primaryColor, secondaryColor));
	}
	
	public static void addMashing(Block input, Block input2, ItemStack output, float xp)
    {
        MashingRecipes.smelting().addMashRecipe(input, input2, output, xp);
    }

    public static void addMashing(Item input, Item input2, ItemStack output, float xp)
    {
        MashingRecipes.smelting().addMashRecipe(input, input2, output, xp);
    }

    public static void addMashing(ItemStack input, ItemStack input2, ItemStack output, float xp)
    {
        MashingRecipes.smelting().addMashRecipe(input, input2, output, xp);
    }
}
