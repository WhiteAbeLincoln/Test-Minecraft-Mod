package com.abe.tutorial.help;

import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.util.EnumHelper;

public class Reference {
	//Mod Info
	public static final String MODID = "abewhite_tutorial";
    public static final String VERSION = "1.0.0";
    public static final String NAME_STRING = "Tutorial Mod";
    
    public static final String NET_CHANNEL_NAME = "TUTORIAL_MOD";
}
