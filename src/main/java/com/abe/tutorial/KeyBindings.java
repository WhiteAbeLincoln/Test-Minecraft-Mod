package com.abe.tutorial;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.client.registry.ClientRegistry;
import net.minecraft.client.settings.KeyBinding;

public class KeyBindings {

    // Declare two KeyBindings, ping and pong
    public static KeyBinding key_explode;
    public static KeyBinding pong;

    public static void init() {
        // Define the "ping" binding, with (unlocalized) name "key.ping" and
        // the category with (unlocalized) name "key.categories.mymod" and
        // key code 24 ("O", LWJGL constant: Keyboard.KEY_O)
        key_explode = new KeyBinding("key.key_explode", Keyboard.KEY_R, "key.categories.mymod");
        // Define the "pong" binding, with (unlocalized) name "key.pong" and
        // the category with (unlocalized) name "key.categories.mymod" and
        // key code 25 ("P", LWJGL constant: Keyboard.KEY_P)
        pong = new KeyBinding("key.pong", Keyboard.KEY_P, "key.categories.mymod");

        // Register both KeyBindings to the ClientRegistry
        ClientRegistry.registerKeyBinding(key_explode);
        ClientRegistry.registerKeyBinding(pong);
    }

}
