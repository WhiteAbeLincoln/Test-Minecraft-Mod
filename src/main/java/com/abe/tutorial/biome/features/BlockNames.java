package com.abe.tutorial.biome.features;

public class BlockNames {

	//logs
	public static final String[] logs = new String[] {"Maple", "Poplar", "Cherry", "Plum", "Apricot"};
	
	//leaves
	public static final String[][] leafTypes = new String[][] {
		{"LeafMaple", "LeafPoplar", "LeafCherry", "LeafCherry", "LeafPlum", "LeafApricot"},
		{"LeafMapleOpaque", "LeafPoplarOpaque", "LeafCherryOpaque", "LeafCherryOpaque", "LeafPlumOpaque", "LeafApricotOpaque"}
	  };
	public static final String[] leaves = logs;//new String[] {"Maple", "Poplar", "Cherry", "Plum", "Apricot"};
	
	//saplings
	public static final String[] saplings = logs;
}
