package com.abe.tutorial.biome.features;

import java.util.List;



import com.abe.tutorial.biome.features.BlockNames;
import com.abe.tutorial.help.Reference;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockLog;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public class TutLog extends BlockLog{
	
	@SideOnly(Side.CLIENT)
	@Override
	public void getSubBlocks(Item item, CreativeTabs tabs, List list) {
		for (int i= 0; i < BlockNames.logs.length; i++){
			list.add(new ItemStack(item, 1, i));
		}
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister iconRegister) {
		this.field_150167_a = new IIcon[BlockNames.logs.length];
		this.field_150166_b = new IIcon[BlockNames.logs.length];
		
		for (int i = 0; i<this.field_150167_a.length; i++){
			this.field_150167_a[i] = iconRegister.registerIcon(Reference.MODID + ":" + this.getUnlocalizedName().substring(5) + BlockNames.logs[i]);
			this.field_150166_b[i] = iconRegister.registerIcon(Reference.MODID + ":" + this.getUnlocalizedName().substring(5) + BlockNames.logs[i] + "Top");
		}
	}
}
