package com.abe.tutorial.core.network;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.INetHandler;
import net.minecraft.network.NetHandlerPlayServer;
import mantle.common.network.AbstractPacket;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.ChannelHandler.Sharable;

@Sharable
public class PacketHandler extends SimpleChannelInboundHandler<AbstractPacket>{

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, AbstractPacket msg) throws Exception {
		
		switch (FMLCommonHandler.instance().getEffectiveSide()) {
        case CLIENT:
            msg.handleClientSide(getClientPlayer());
            break;
        case SERVER:
            INetHandler netHandler = ctx.channel().attr(NetworkRegistry.NET_HANDLER).get();
            msg.handleServerSide(getServerPlayer(netHandler));
            break;
		}
	}
	
	@SideOnly(Side.CLIENT)
	public EntityPlayer getClientPlayer() {
		return Minecraft.getMinecraft().thePlayer;
	}
	
	public EntityPlayer getServerPlayer(INetHandler netHandler){
		EntityPlayer playerEntity = ((NetHandlerPlayServer) netHandler).playerEntity;
		
		return playerEntity;
	}

}
