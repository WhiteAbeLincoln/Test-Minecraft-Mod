package com.abe.tutorial.core.network;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import com.abe.tutorial.TutLogger;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.INetHandler;
import net.minecraft.network.NetHandlerPlayServer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import mantle.common.network.AbstractPacket;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.FMLEmbeddedChannel;
import cpw.mods.fml.common.network.FMLIndexedMessageToMessageCodec;
import cpw.mods.fml.common.network.FMLOutboundHandler;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.FMLOutboundHandler.OutboundTarget;
import cpw.mods.fml.relauncher.Side;

public class TutorialChannelHandler extends FMLIndexedMessageToMessageCodec<AbstractPacket>{

	private static final Map<String, TutorialChannelHandler> TRACKER = new HashMap<String, TutorialChannelHandler>();
	public final String channel;
	public EnumMap<Side, FMLEmbeddedChannel> channels;
	
	public TutorialChannelHandler(String pluginID, Class<? extends AbstractPacket>... packetClasses){
		this.channel = pluginID.toUpperCase();
		
		ArrayList<Class<? extends AbstractPacket>> list = new ArrayList<Class<? extends AbstractPacket>>();
		for (int i = 0; i < packetClasses.length; i++){
			if (!list.contains(packetClasses[i])) {
				list.add(packetClasses[i]);
			} else {
				TutLogger.logger.log(Level.WARNING, "Channel "+ channel + " has already registered message/packet class " + packetClasses[i].getSimpleName());
			}
			
			this.addDiscriminator(i, packetClasses[i]);
			
		}
		
	}
	
	public static boolean registerHandler(String pluginID,
			Class<? extends AbstractPacket>... packetClasses) {
		if (!TutorialChannelHandler.TRACKER.containsKey(pluginID.toUpperCase())) {
			TutorialChannelHandler handler = new TutorialChannelHandler(pluginID, packetClasses);
			
			handler.channels = NetworkRegistry.INSTANCE.newChannel(pluginID.toUpperCase(), handler);
			
			PacketHandler executer = new PacketHandler();
			
			for (Map.Entry<Side, FMLEmbeddedChannel> e : handler.channels.entrySet()) {
				FMLEmbeddedChannel channel = e.getValue();
				String codec = channel.findChannelHandlerNameForType(PacketHandler.class);
				channel.pipeline().addAfter(codec, "PacketHandler", executer);
			}
			
			TutorialChannelHandler.TRACKER.put(pluginID.toUpperCase(), handler);
			
			return true;
		}
		else {
			TutLogger.logger.log(Level.WARNING, "There is already a channel/handler for key/channel " + pluginID.toUpperCase());
			return false;
		}
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, AbstractPacket msg, ByteBuf target) throws Exception {
		try {
			msg.encodeInto(ctx, target);
		} catch (Exception e) {
			TutLogger.logger.log(Level.SEVERE, "Error writing to packet for channel: " + channel);
			e.printStackTrace();
		}
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf source, AbstractPacket msg) {
		try {
			msg.decodeInto(ctx, source);
		} catch (Exception e) {
			TutLogger.logger.log(Level.SEVERE, "Error reading from packet for channel: " + channel);
			e.printStackTrace();
		}
		
	}
	
	public static boolean sendToServer(String channel, AbstractPacket packet) {
		if (TutorialChannelHandler.TRACKER.containsKey(channel.toUpperCase())) {
			EnumMap<Side, FMLEmbeddedChannel> channels = TutorialChannelHandler.TRACKER.get(channel
					.toUpperCase()).channels;
			
			channels.get(Side.CLIENT).attr(FMLOutboundHandler.FML_MESSAGETARGET)
					.set(FMLOutboundHandler.OutboundTarget.TOSERVER);
			channels.get(Side.CLIENT).writeAndFlush(packet);
			
			return true;
		}
		return false;
	}
    
	public static boolean sendToAllAround(String channel, AbstractPacket packet, NetworkRegistry.TargetPoint point) {
		if (TutorialChannelHandler.TRACKER.containsKey(channel.toUpperCase())) {
			EnumMap<Side, FMLEmbeddedChannel> channels = TutorialChannelHandler.TRACKER.get(channel
					.toUpperCase()).channels;
			
			channels.get(Side.SERVER).attr(FMLOutboundHandler.FML_MESSAGETARGET)
					.set(FMLOutboundHandler.OutboundTarget.ALLAROUNDPOINT);
			channels.get(Side.SERVER).attr(FMLOutboundHandler.FML_MESSAGETARGETARGS).set(point);
			channels.get(Side.SERVER).writeAndFlush(packet);
			
			return true;
		}
		return false;
	}
	
	public static boolean sendToAll(String channel, AbstractPacket packet) {
		if (TutorialChannelHandler.TRACKER.containsKey(channel.toUpperCase())) {
			EnumMap<Side, FMLEmbeddedChannel> channels = TutorialChannelHandler.TRACKER.get(channel
					.toUpperCase()).channels;
			
			channels.get(Side.SERVER).attr(FMLOutboundHandler.FML_MESSAGETARGET)
					.set(FMLOutboundHandler.OutboundTarget.ALL);
			channels.get(Side.SERVER).writeAndFlush(packet);
			
			return true;
		}
		return false;
	}
    
    public static boolean sendToPlayer(String channel, AbstractPacket packet, EntityPlayer player) {
		if (TutorialChannelHandler.TRACKER.containsKey(channel.toUpperCase())) {
			EnumMap<Side, FMLEmbeddedChannel> channels = TutorialChannelHandler.TRACKER.get(channel
					.toUpperCase()).channels;
			
			channels.get(Side.SERVER).attr(FMLOutboundHandler.FML_MESSAGETARGET)
					.set(FMLOutboundHandler.OutboundTarget.PLAYER);
			channels.get(Side.SERVER).attr(FMLOutboundHandler.FML_MESSAGETARGETARGS).set(player);
			channels.get(Side.SERVER).writeAndFlush(packet);
			
			return true;
		}
		return false;
	}
    
    public static boolean sendToDimension(String channel, AbstractPacket packet, int dimension) {
		if (TutorialChannelHandler.TRACKER.containsKey(channel.toUpperCase())) {
			EnumMap<Side, FMLEmbeddedChannel> channels = TutorialChannelHandler.TRACKER.get(channel
					.toUpperCase()).channels;
			
			channels.get(Side.SERVER).attr(FMLOutboundHandler.FML_MESSAGETARGET)
					.set(FMLOutboundHandler.OutboundTarget.DIMENSION);
			channels.get(Side.SERVER).attr(FMLOutboundHandler.FML_MESSAGETARGETARGS).set(dimension);
			channels.get(Side.SERVER).writeAndFlush(packet);
			
			return true;
		}
		return false;
	}

}
