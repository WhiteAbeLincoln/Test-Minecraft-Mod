package com.abe.tutorial.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import org.lwjgl.opengl.GL11;

import com.abe.tutorial.container.ContainerTestBlock;
import com.abe.tutorial.container.ContainerWorkSurface;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.tileEntity.TileEntityTestBlock;
import com.abe.tutorial.tileEntity.TileEntityWorkSurface;

public class GuiTestBlock extends GuiContainer{

	private ResourceLocation texture = new ResourceLocation(Reference.MODID + ":" + "textures/gui/WorkSurface.png");
	private TileEntityTestBlock workSurface;
	public GuiTestBlock(InventoryPlayer inventoryPlayer, TileEntityTestBlock tileEntity, World world, int x, int y, int z) {
		super(tileEntity.getGuiContainer(inventoryPlayer, world, x, y, z));
		workSurface = tileEntity;
		
		this.xSize = 176;
		this.ySize = 188;
	}
	
	public void onGuiClosed(){
		super.onGuiClosed();
	}
	
	protected void drawGuiContainerForeground(int i, int j){
		String name = this.workSurface.hasCustomInventoryName() ? this.workSurface.getInventoryName() : I18n.format(this.workSurface.getInventoryName());
		//this.fontRendererObj.drawString(StatCollector.translateToLocal("Work Surface"), 100, 5, 0x000000);
		this.fontRendererObj.drawString(name, Math.abs((this.xSize / 2) - this.fontRendererObj.getStringWidth(name) / 2), 6, 4210752);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2,
			int var3) {
		GL11.glColor4f(1F, 1F, 1F, 1F);
		Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
		
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	}

}
