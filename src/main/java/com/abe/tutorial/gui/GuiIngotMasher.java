package com.abe.tutorial.gui;

import org.lwjgl.opengl.GL11;

import com.abe.tutorial.container.ContainerIngotMasher;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.tileEntity.TileEntityIngotMasher;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;

public class GuiIngotMasher extends GuiContainer{

	private ResourceLocation texture = new ResourceLocation(Reference.MODID + ":" + "/textures/gui/GuiIngotMasher.png");
	private TileEntityIngotMasher ingotMasher;
	
	public GuiIngotMasher(InventoryPlayer inventoryPlayer, TileEntityIngotMasher tileEntityIngotMasher) {
		super(new ContainerIngotMasher(inventoryPlayer, tileEntityIngotMasher));
		ingotMasher = tileEntityIngotMasher;
		
		this.xSize = 176;
		this.ySize = 166;
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int i, int j) {
		String name = this.ingotMasher.hasCustomInventoryName() ? this.ingotMasher.getInventoryName() : I18n.format(this.ingotMasher.getInventoryName());
		
		this.fontRendererObj.drawString(name, Math.abs((this.xSize / 2) - this.fontRendererObj.getStringWidth(name) / 2), 6, 4210752);
		this.fontRendererObj.drawString(I18n.format("container.inventory"), 8, this.ySize - 96 + 5, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2,
			int var3) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
		
		//power gauge
		if (ingotMasher.hasPower()){
			int i1 = ingotMasher.getPowerRemainingScaled(45);
			drawTexturedModalRect(guiLeft+8, guiTop+53-i1, 176, 89-i1, 44, i1);
		}
		
		//TODO mashing
		if (ingotMasher.isMashing() && ingotMasher.canMash()){
			int j1 = ingotMasher.getMasherProgressScaled(44);
			drawTexturedModalRect(guiLeft + 63, guiTop + 19, 0, 166, j1 + 1, 44);
		}
	}

}
