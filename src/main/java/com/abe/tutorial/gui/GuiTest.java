package com.abe.tutorial.gui;

import io.netty.buffer.Unpooled;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import com.abe.tutorial.TutorialMod;
import com.abe.tutorial.container.ContainerStructureController;
import com.abe.tutorial.core.network.NetworkUtil;
import com.abe.tutorial.core.network.PacketUpdate;
import com.abe.tutorial.core.network.TutorialChannelHandler;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.tileEntity.TileEntityStructureController;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiCommandBlock;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.play.client.C17PacketCustomPayload;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class GuiTest extends GuiContainer {

	private static final ResourceLocation BGROUND_LOCATION = new ResourceLocation(Reference.MODID + ":" + "textures/gui/GuiStructureController.png");
	public TileEntityStructureController structureController;
	private GuiButton doneBtn;
    private GuiButton cancelBtn;
    private GuiTextField structureWidth;
    private GuiTextField structureHeight;
    
    public GuiTest(InventoryPlayer iPlayer, TileEntityStructureController controller){
    	super(new ContainerStructureController(iPlayer, controller));
    	
    	this.structureController = controller;
    	System.out.println("Opened");
    	
    	this.xSize = 176;
		this.ySize = 166;
    }
    
    @Override
    protected void drawGuiContainerForegroundLayer(int par1, int par2) {
    	String name = this.structureController.hasCustomInventoryName() ? this.structureController.getInventoryName() : I18n.format(this.structureController.getInventoryName());
    	
    	this.fontRendererObj.drawString(name, this.xSize / 2 - this.fontRendererObj.getStringWidth(name) / 2, 6, 4210752);
		this.fontRendererObj.drawString(I18n.format("container.inventory", new Object[0]), 118, this.ySize - 96 + 2, 4210752);
		
		this.fontRendererObj.drawString((I18n.format("options.chat.height.focused", new Object[0]).split(" "))[1], this.xSize - 60, 20, 4210752);
        this.fontRendererObj.drawString(I18n.format("options.chat.width", new Object[0]), this.xSize - 60, 42, 4210752);
        this.structureHeight.drawTextBox();
        this.structureWidth.drawTextBox();
    }
    
    @Override
    protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
    	GL11.glColor4f(1F, 1F, 1F, 1F);
    	
    	Minecraft.getMinecraft().getTextureManager().bindTexture(BGROUND_LOCATION);
    	drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
    }
    
   @Override
    public void updateScreen() {
	   super.updateScreen();
    	this.structureWidth.updateCursorCounter();
    	this.structureHeight.updateCursorCounter();
    }
    
    @Override
    public void initGui() {
    	super.initGui();
    	Keyboard.enableRepeatEvents(true);
    	this.buttonList.clear();
    	this.buttonList.add(this.doneBtn = new GuiButton(0, 100, this.ySize / 2, 40, 20, I18n.format("gui.done", new Object[0])));
        this.structureHeight = new GuiTextField(this.fontRendererObj, this.xSize - 28, 17, 20, 20);
        this.structureHeight.setFocused(true);
        this.structureWidth = new GuiTextField(this.fontRendererObj, this.xSize - 28, this.ySize / 4, 20, 20);
        this.structureHeight.setMaxStringLength(2);
        this.structureWidth.setMaxStringLength(2);
        
        this.doneBtn.enabled = true;
    }
	
	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
		super.onGuiClosed();
	}
	
	@Override
	protected void actionPerformed(GuiButton button) {
		if (button.enabled){			
			switch (button.id) {
			case 0:
				System.out.println("ButtonCLikcked");
				NBTTagCompound nbt = new NBTTagCompound();
				nbt.setBoolean("ClickButton", true);
				
				
				
				TutorialChannelHandler.sendToAll(Reference.NET_CHANNEL_NAME, new PacketUpdate(structureController.xCoord, structureController.yCoord, structureController.zCoord, nbt));

                //this.mc.displayGuiScreen((GuiScreen)null);
				break;
			case 1:
				this.mc.displayGuiScreen((GuiScreen)null);
				break;
			default:
				break;
			}
			
			
		}
	}
	
	@Override
	protected void keyTyped(char letter, int key) {
		super.keyTyped(letter, key);
		this.structureHeight.textboxKeyTyped(letter, key);
        this.structureWidth.textboxKeyTyped(letter, key);
        this.doneBtn.enabled = true; //this.structureHeight.getText().trim().length() > 0 && this.structureWidth.getText().trim().length() > 0;
	}
	
	 /**
     * Called when the mouse is clicked.
     */
    protected void mouseClicked(int par1, int par2, int par3)
    {
        this.structureHeight.mouseClicked(par1, par2, par3);
        this.structureWidth.mouseClicked(par1, par2, par3);
        super.mouseClicked(par1, par2, par3);
    }

}
