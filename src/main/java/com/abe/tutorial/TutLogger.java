package com.abe.tutorial;

import java.lang.reflect.Method;
import java.util.logging.Logger;

public class TutLogger {

	public static final Logger logger = Logger.getLogger("TutorialMod");
	
	/**
	 * Deactivate constructor
	 */
	private TutLogger() {
	}
	
	public static void initLog(){
		logger.info("Starting TutorialMod " + "0.1");
		logger.info("Copyright (c) Abe, 2014");
		logger.info("abelincoln.white@gmail.com");
	}
	
}
