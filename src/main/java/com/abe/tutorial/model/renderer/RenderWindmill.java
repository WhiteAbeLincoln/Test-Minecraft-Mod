package com.abe.tutorial.model.renderer;

import org.lwjgl.opengl.GL11;

import com.abe.tutorial.help.Reference;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class RenderWindmill extends TileEntitySpecialRenderer {

	private final ResourceLocation textureWindmill = new ResourceLocation(Reference.MODID + ":" + "textures/model/TextureWindmill.png");
	
	private int textureWidth = 64;
	private int textureHeight = 32;
	
	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double distanceX, double distanceY, double distanceZ, float f) {
		//System.out.println("RENDERING BLOCK WINDMILL");
		GL11.glPushMatrix();
		{
			GL11.glTranslatef((float)distanceX, (float)distanceY, (float)distanceZ);
			Tessellator tessellator = Tessellator.instance;		//can only use one tessalator at a time (close it!!)
			this.bindTexture(textureWindmill);
			
			tessellator.startDrawingQuads();	//begin
			{
				//(x, y, z, textX, textY)
				tessellator.addVertexWithUV(0, 0, 1, 1, 1);
				tessellator.addVertexWithUV(0, 10, 1, 1, 0);
				tessellator.addVertexWithUV(0, 10, 0, 0, 0);
				tessellator.addVertexWithUV(0, 0, 0, 0, 1);
				
			}
			tessellator.draw();		//end
			tessellator.startDrawingQuads();
			{
				tessellator.addVertexWithUV(1, 0, 1, 0, 1);
				tessellator.addVertexWithUV(1, 10, 1, 0, 0);
				tessellator.addVertexWithUV(0, 10, 1, 1, 1/3);
				tessellator.addVertexWithUV(0, 0, 1, 1, 1/4);
				
			}
			tessellator.draw();
			tessellator.startDrawingQuads();
			{
				tessellator.addVertexWithUV(0, 0, 0, 0, 1);
				tessellator.addVertexWithUV(0, 10, 0, 0, 0);
				tessellator.addVertexWithUV(1, 10, 0, 1, 1/3);
				tessellator.addVertexWithUV(1, 0, 0, 1, 1/4);
				
			}
			tessellator.draw();
			tessellator.startDrawingQuads();	//begin
			{
				//(x, y, z, textX, textY)
				tessellator.addVertexWithUV(1, 0, 0, 1, 1);
				tessellator.addVertexWithUV(1, 10, 0, 1, 0);
				tessellator.addVertexWithUV(1, 10, 1, 0, 0);
				tessellator.addVertexWithUV(1, 0, 1, 0, 1);
				
			}
			tessellator.draw();
		}
		GL11.glPopMatrix();
	}

}
