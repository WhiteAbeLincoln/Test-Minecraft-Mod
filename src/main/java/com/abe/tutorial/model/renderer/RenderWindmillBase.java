package com.abe.tutorial.model.renderer;

import org.lwjgl.opengl.GL11;

import com.abe.tutorial.blocks.RegisterBlocks;
import com.abe.tutorial.help.Reference;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class RenderWindmillBase extends TileEntitySpecialRenderer{

	private final ResourceLocation textureWindmill = new ResourceLocation(Reference.MODID + ":" + "textures/model/TextureWindmillBigBase1.png");
	
	private int textureWidth = 96;
	private int bWidth = textureWidth/3;
	private int textureHeight = 96;
	private int bHeight = textureHeight/3;
	
	private float pixel = 1F/16F;
	private float blockY= 4.9F*pixel;
	
	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double distX, double distY, double distZ, float f) {
		GL11.glPushMatrix();
		{
			GL11.glDisable(GL11.GL_LIGHTING);
			GL11.glTranslatef((float)distX, (float)distY, (float)distZ);
			Tessellator tessellator = Tessellator.instance;		//can only use one tessalator at a time (close it!!)
			this.bindTexture(textureWindmill);
			
			tessellator.startDrawingQuads();	//begin
			{
				  /*tessellator.addVertexWithUV(0, 5*pixel, 1, 0, 0);
					tessellator.addVertexWithUV(1, 5*pixel, 1, 0, 1);
					tessellator.addVertexWithUV(1, 5*pixel, 0, 1, 1);
					tessellator.addVertexWithUV(0, 5*pixel, 0, 1, 0);*/
				
				if(tileEntity.getWorldObj().getBlockMetadata(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord) == 1){
					tessellator.addVertexWithUV(0, blockY, 1, 0, -1F/textureHeight*bHeight);
					tessellator.addVertexWithUV(1, blockY, 1, 0, 0);
					tessellator.addVertexWithUV(1, blockY, 0, 1F/textureWidth*bWidth, 0);
					tessellator.addVertexWithUV(0, blockY, 0, 1F/textureWidth*bWidth, -1F/textureHeight*bHeight);
				}
				if(tileEntity.getWorldObj().getBlockMetadata(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord) == 2){
					//(x, y, z, textX, textY)
					tessellator.addVertexWithUV(0, blockY, 1, 1F/textureWidth*bWidth, -1F/textureHeight*bHeight);
					tessellator.addVertexWithUV(1, blockY, 1, 1F/textureWidth*bWidth, 0);
					tessellator.addVertexWithUV(1, blockY, 0, 1F/textureWidth*(bWidth*2), 0);
					tessellator.addVertexWithUV(0, blockY, 0, 1F/textureWidth*(bWidth*2), -1F/textureHeight*bHeight);
					
				}
				if(tileEntity.getWorldObj().getBlockMetadata(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord) == 3){
					//(x, y, z, textX, textY)
					tessellator.addVertexWithUV(0, blockY, 1, 1F/textureWidth*(bWidth*2), -1F/textureWidth*bHeight);
					tessellator.addVertexWithUV(1, blockY, 1, 1F/textureWidth*(bWidth*2), 0);
					tessellator.addVertexWithUV(1, blockY, 0, 1, 0);
					tessellator.addVertexWithUV(0, blockY, 0, 1, -1F/textureWidth*bHeight);
					
				}
				if(tileEntity.getWorldObj().getBlockMetadata(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord) == 4){
					//(x, y, z, textX, textY)
					tessellator.addVertexWithUV(0, blockY, 1, 0, 1F/textureHeight*bHeight);
					tessellator.addVertexWithUV(1, blockY, 1, 0, 1F/textureHeight*(bHeight*2));
					tessellator.addVertexWithUV(1, blockY, 0, 1F/textureWidth*bWidth, 1F/textureHeight*(bHeight*2));
					tessellator.addVertexWithUV(0, blockY, 0, 1F/textureWidth*bWidth, 1F/textureHeight*bHeight);
					
				}
				if(tileEntity.getWorldObj().getBlockMetadata(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord) == 5){
					//(x, y, z, textX, textY)
					tessellator.addVertexWithUV(0, blockY, 1, 1F/textureWidth*bWidth, 1F/textureHeight*bHeight);
					tessellator.addVertexWithUV(1, blockY, 1, 1F/textureWidth*bWidth, 1F/textureHeight*(bHeight*2));
					tessellator.addVertexWithUV(1, blockY, 0, 1F/textureWidth*(bWidth*2), 1F/textureHeight*(bHeight*2));
					tessellator.addVertexWithUV(0, blockY, 0, 1F/textureWidth*(bWidth*2), 1F/textureHeight*bHeight);
					
				}
				if(tileEntity.getWorldObj().getBlockMetadata(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord) == 6){
					//(x, y, z, textX, textY)
					tessellator.addVertexWithUV(0, blockY, 1, 1F/textureWidth*(bWidth*2), 1F/textureHeight*bHeight);
					tessellator.addVertexWithUV(1, blockY, 1, 1F/textureWidth*(bWidth*2), 1F/textureHeight*(bHeight*2));
					tessellator.addVertexWithUV(1, blockY, 0, 1, 1F/textureHeight*(bHeight*2));
					tessellator.addVertexWithUV(0, blockY, 0, 1, 1F/textureHeight*bHeight);
					
				}
				if(tileEntity.getWorldObj().getBlockMetadata(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord) == 7){
					tessellator.addVertexWithUV(0, blockY, 1, 0, 0);
					tessellator.addVertexWithUV(1, blockY, 1, 0, 1F/textureHeight*bHeight);
					tessellator.addVertexWithUV(1, blockY, 0, 1F/textureWidth*bWidth, 1F/textureHeight*bHeight);
					tessellator.addVertexWithUV(0, blockY, 0, 1F/textureWidth*bWidth, 0);
				}
				if(tileEntity.getWorldObj().getBlockMetadata(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord) == 8){
					tessellator.addVertexWithUV(0, blockY, 1, 1F/textureWidth*bWidth, 0);
					tessellator.addVertexWithUV(1, blockY, 1, 1F/textureWidth*bWidth, 1F/textureHeight*bHeight);
					tessellator.addVertexWithUV(1, blockY, 0, 1F/textureWidth*(bWidth*2), 1F/textureHeight*bHeight);
					tessellator.addVertexWithUV(0, blockY, 0, 1F/textureWidth*(bWidth*2), 0);
				}
				if(tileEntity.getWorldObj().getBlockMetadata(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord) == 9){
					//(x, y, z, textX, textY)
					tessellator.addVertexWithUV(0, blockY, 1, 1F/textureWidth*(bWidth*2), 0);
					tessellator.addVertexWithUV(1, blockY, 1, 1F/textureWidth*(bWidth*2), 1F/textureWidth*bHeight);
					tessellator.addVertexWithUV(1, blockY, 0, 1, 1F/textureWidth*bHeight);
					tessellator.addVertexWithUV(0, blockY, 0, 1, 0);
					
				}
				
			}
			tessellator.draw();		//end
			GL11.glEnable(GL11.GL_LIGHTING);
		}
		GL11.glPopMatrix();
	}

}
