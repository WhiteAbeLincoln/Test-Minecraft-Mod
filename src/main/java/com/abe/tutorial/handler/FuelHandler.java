package com.abe.tutorial.handler;

import com.abe.tutorial.items.RegisterItems;

import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.IFuelHandler;

public class FuelHandler implements IFuelHandler {

	@Override
	public int getBurnTime(ItemStack fuel) {
		
		if (fuel.getItem() == RegisterItems.itemTreePitch) return RegisterItems.itemTreePitchBurnTime;
		
		return 0;
	}

}
