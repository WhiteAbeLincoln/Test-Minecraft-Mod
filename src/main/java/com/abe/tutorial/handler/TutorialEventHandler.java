package com.abe.tutorial.handler;

import com.abe.tutorial.RegisterAchievements;
import com.abe.tutorial.blocks.RegisterBlocks;
import com.abe.tutorial.items.RegisterItems;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingSetAttackTargetEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;
import net.minecraftforge.event.world.BlockEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.ItemPickupEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.ItemSmeltedEvent;

public class TutorialEventHandler {
	
	@SubscribeEvent
	public void checkUpdate(PlayerEvent.PlayerLoggedInEvent event){
		event.player.addChatMessage(new ChatComponentText("Hello World!"));
	}
	
	/*@SubscribeEvent
	public void throwEgg(PlayerInteractEvent event){
		
	}
	
	@SubscribeEvent
	public void swingSword(PlayerInteractEvent event){
		if (event.entityPlayer.getCurrentEquippedItem() != null){
			if (event.entityPlayer.getCurrentEquippedItem().getItem().equals(ModItems.tutorialSword)) {
			}
		}
	}*/
	
	@SubscribeEvent
	public void somethingSmelted(ItemSmeltedEvent event){
		
		if (event.smelting.getItem() == RegisterItems.itemUnstableIngot){
			event.player.addChatMessage(new ChatComponentText(event.player.getDisplayName().toString()+" smelted a tutorial Ingot!"));
		}
	}
	
	@SubscribeEvent
	public void smeltedItem(PlayerEvent.ItemSmeltedEvent e) {
		if (e.smelting.getItem().equals(RegisterItems.itemUnstableIngot)) {
			//adds an achievement
			//params are... (achievement to add, integer 1 or 0)   1 = add achievment, 0 = take away?
			e.player.addStat(RegisterAchievements.smeltTutorialIngotAchievement, 1);
		}
	}
	
	@SubscribeEvent
	public void minedItem(PlayerEvent.ItemPickupEvent e) {
		if (Block.getBlockFromItem(e.pickedUp.getEntityItem().getItem()).equals(RegisterBlocks.oreUnstableOre)) {
			System.out.println("Mined Ore");
			//adds an achievement
			//params are... (achievement to add, integer 1 or 0)   1 = add achievment, 0 = take away?
			e.player.addStat(RegisterAchievements.mineTutorialOreAchievement, 1);
		}
	}
}
