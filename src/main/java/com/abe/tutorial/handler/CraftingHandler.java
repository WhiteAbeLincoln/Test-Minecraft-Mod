package com.abe.tutorial.handler;

import com.abe.tutorial.items.RegisterItems;
import com.abe.tutorial.util.CreationTools;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.ItemCraftedEvent;

public class CraftingHandler {
	
	@SubscribeEvent
	public void onCrafting(ItemCraftedEvent event){
		
		final IInventory craftMatrix = null;
		
		//parses through the crafting table
		for (int i = 0; i < event.craftMatrix.getSizeInventory(); i++){
			/*if (event.craftMatrix.getStackInSlot(i) != null){	//if the stack at i is not null
				/*ItemStack item0 = event.craftMatrix.getStackInSlot(i);		//gets the stack
				
				if (item0 != null && item0.getItem() == RegisterItems.itemIronHammer){		//if desired Item
					ItemStack k = new ItemStack(RegisterItems.itemIronHammer, 2, (item0.getItemDamage() + 1));		//sets a new itemstack with 1 more damage
					
					if (k.getItemDamage() >= k.getMaxDamage()){		//if at max damage
						k.stackSize--;		//remove stack
					}
					event.craftMatrix.setInventorySlotContents(i, k);		//sets the stack back into the slot
				}*/
				
				
			//uses a method that I wrote for easy use
				CreationTools.craftDurableItem(event, i, RegisterItems.itemIronHammer, 1);
				CreationTools.craftDurableItem(event, i, RegisterItems.itemIronPunch, 1);
		}
	}	
}
	

