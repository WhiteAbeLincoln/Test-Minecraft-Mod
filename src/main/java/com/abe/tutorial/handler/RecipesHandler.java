package com.abe.tutorial.handler;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

import com.abe.tutorial.blocks.RegisterBlocks;
import com.abe.tutorial.crafting.CustomFurnaceRecipes;
import com.abe.tutorial.crafting.WorkSurfaceCraftingManager;
import com.abe.tutorial.help.RegisterHelper;
import com.abe.tutorial.items.RegisterItems;
import com.abe.tutorial.util.RecipeRemover;

import cpw.mods.fml.common.registry.GameRegistry;

public class RecipesHandler {
	
	public static Map<ItemStack, ItemStack> alabasterSmeltMap = new HashMap();
	public static Map alabasterXPMap = new HashMap();
	
	
	private static CustomFurnaceRecipes alabasterOvenRecipes = new CustomFurnaceRecipes(alabasterSmeltMap, alabasterXPMap);
	public static void addRecipes() {
		
		GameRegistry.addShapelessRecipe(new ItemStack(RegisterItems.itemUnstableIngot), new Object[]{
    		Items.iron_ingot, new ItemStack(Items.dye, 1, 1), Items.diamond
    	});
    	
    	GameRegistry.addRecipe(new ItemStack(Items.apple, 4), new Object[]{
    		"AAA",		//each set of three A's represents a line of the crafting table with each A being one spot.
    		"AAA",
    		"AAA",
    		'A',Items.cookie		//the A in single quotes tells what the Previous A's represent, with the next comma delimited item being what that value is.
    								//in this case, this recipe says that you fill the entire crafting table with cookies, and you should get a block of obsidian
    		});
    	
    	GameRegistry.addRecipe(new ItemStack(RegisterBlocks.blockAlabasterOvenIdle), new Object[]{
    		"AAA",
    		"A A",
    		"AAA",
    		'A', RegisterBlocks.oreAlabasterBlock
    	});
    	
    	//Sword
    	GameRegistry.addRecipe(new ItemStack(RegisterItems.itemUnstableSword, 1), new Object[]{
        	"A",
        	"A",
        	"B",
        	'A', RegisterItems.itemUnstableIngot,
        	'B', Items.stick
    	});
    	
    	GameRegistry.addRecipe(new ItemStack(RegisterItems.itemUnstableAxe), new Object[]{
        	" AA",
        	" BA",
        	" B ",
        	'A', RegisterItems.itemUnstableIngot,
        	'B', Items.stick
    	});
    	
    	GameRegistry.addRecipe(new ItemStack(RegisterItems.itemUnstableAxe), new Object[]{
        	"AA ",
        	"AB ",
        	" B ",
        	'A', RegisterItems.itemUnstableIngot,
        	'B', Items.stick
    	});
    	
    	GameRegistry.addRecipe(new ItemStack(RegisterItems.itemUnstablePickaxe), new Object[]{
    		"AAA",
    		" B ",
    		" B ",
    		'A', RegisterItems.itemUnstableIngot,
    		'B', Items.stick
    	});
    	
    	GameRegistry.addRecipe(new ItemStack(RegisterItems.itemUnstableShovel), new Object[]{
    		"A",
    		"B",
    		"B",
    		'A', RegisterItems.itemUnstableIngot,
    		'B', Items.stick 
    	});
    	
    	//unstable block
    	GameRegistry.addRecipe(new ItemStack(RegisterBlocks.blockUnstableBlock), new Object[]{
    		"AAA",
    		"AAA",
    		"AAA",
    		'A', RegisterItems.itemUnstableIngot
    	});
    	
    	GameRegistry.addRecipe(new ItemStack(RegisterItems.itemTinCog, 4), new Object[]{
    		" A ",
    		"AFA",
    		" A ",
    		'A', RegisterItems.itemTinIngot,
    		'F', Items.flint
    	});
    	
    	GameRegistry.addRecipe(new ItemStack(RegisterBlocks.blockObsidianTable), new Object[]{
        	"AAA",
        	"B B",
        	'A', Blocks.obsidian,
        	'B', Blocks.iron_block
    	});
    	
    	WorkSurfaceCraftingManager.getInstance().addRecipe(new ItemStack(RegisterBlocks.blockWorkSurface), new Object[]{
    		"AAAAA",
    		"A R A",
    		"ASRSA",
    		"A R A",
    		"AAAAA",
    		'A', Blocks.iron_block,
    		'R', Items.redstone,
    		'S', RegisterItems.itemSiliconChunk
    	});
    	
    	GameRegistry.addShapelessRecipe(new ItemStack(RegisterBlocks.oreUnstableOre), new Object[]{RegisterItems.itemUnstableIngot, Blocks.cobblestone});
    	GameRegistry.addShapelessRecipe(new ItemStack(RegisterItems.itemUnstableIngot, 9), new Object[]{RegisterBlocks.blockUnstableBlock});
    	
    	//durable items
    	GameRegistry.addRecipe(new ItemStack(RegisterItems.itemIronDisc, 4), new Object[]{
    		"IH",
    		'I', Items.iron_ingot,
    		'H', new ItemStack(RegisterItems.itemIronHammer, 1, OreDictionary.WILDCARD_VALUE) 	//wildcard allows multiple uses
    	});
    	
    	GameRegistry.addRecipe(new ItemStack(RegisterItems.itemIronWasher, 1), new Object[]{
    		"DP",
    		'D', RegisterItems.itemIronDisc,
    		'P', new ItemStack(RegisterItems.itemIronPunch, 1, OreDictionary.WILDCARD_VALUE) 	//wildcard allows multiple uses
    	});
    	
    	GameRegistry.addRecipe(new ItemStack(Items.book, 1), new Object[]{
    		"LLL",
    		"PPP",
    		"LLL",
    		'P',Items.paper,
    		'L', Items.leather
    	});
    	
    	RecipeRemover.removeRecipe(Item.getItemFromBlock(Blocks.furnace));
    	RecipeRemover.removeRecipe(Items.book);
    	RecipeRemover.removeRecipe(Items.bread);
	}
	
	public static void addSmelting(){
		GameRegistry.addSmelting(RegisterBlocks.oreUnstableOre, new ItemStack(RegisterItems.itemUnstableIngot), 0.8F);
		GameRegistry.addSmelting(RegisterBlocks.oreManganeseOreBlock, new ItemStack(RegisterItems.itemManganeseIngot), 0F);
		GameRegistry.addSmelting(RegisterBlocks.oreNickelOreBlock, new ItemStack(RegisterItems.itemNickelIngot), 0F);
		GameRegistry.addSmelting(RegisterBlocks.oreRhodiumOreBlock, new ItemStack(RegisterItems.itemRhodiumIngot), 0F);
		GameRegistry.addSmelting(RegisterBlocks.oreTinOreBlock, new ItemStack(RegisterItems.itemTinIngot), 0F);
		GameRegistry.addSmelting(RegisterBlocks.oreVanadiumOreBlock, new ItemStack(RegisterItems.itemVanadiumIngot), 0F);
		GameRegistry.addSmelting(RegisterBlocks.oreZincOreBlock, new ItemStack(RegisterItems.itemZincIngot), 0F);
		alabasterOvenRecipes.addSmeltingRecipe(RegisterBlocks.oreUnstableOre, new ItemStack(RegisterItems.itemBattery), 0.0F);
	}
	
	public static void addMashing(){
		RegisterHelper.addMashing(RegisterItems.itemTinIngot, RegisterItems.itemManganeseIngot, new ItemStack(RegisterItems.itemRhodiumIngot), 0F);
	}

}
