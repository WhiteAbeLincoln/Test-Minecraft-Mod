package com.abe.tutorial.handler;

import com.abe.tutorial.blocks.RegisterBlocks;
import com.abe.tutorial.container.ContainerAlabasterOven;
import com.abe.tutorial.container.ContainerIngotMasher;
import com.abe.tutorial.container.ContainerStructureController;
import com.abe.tutorial.container.ContainerTestBlock;
import com.abe.tutorial.container.ContainerWorkSurface;
import com.abe.tutorial.gui.GuiAlabasterOven;
import com.abe.tutorial.gui.GuiIngotMasher;
import com.abe.tutorial.gui.GuiTest;
import com.abe.tutorial.gui.GuiTestBlock;
import com.abe.tutorial.gui.GuiWorkSurface;
import com.abe.tutorial.tileEntity.TileEntityAlabasterOven;
import com.abe.tutorial.tileEntity.TileEntityIngotMasher;
import com.abe.tutorial.tileEntity.TileEntityStructureController;
import com.abe.tutorial.tileEntity.TileEntityTestBlock;
import com.abe.tutorial.tileEntity.TileEntityWorkSurface;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler{

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity entity = world.getTileEntity(x, y, z);
		
		if (entity != null) {
			switch (ID) {
			case RegisterBlocks.guiIDAlabasterOven:
				if (entity instanceof TileEntityAlabasterOven) {
					return new ContainerAlabasterOven(player.inventory, (TileEntityAlabasterOven) entity);
				}
				return null;
			case RegisterBlocks.guiIDIngotMasher:
				if (entity instanceof TileEntityIngotMasher){
					return new ContainerIngotMasher(player.inventory, (TileEntityIngotMasher)entity);
				}
				return null;
			case RegisterBlocks.guiIDWorkSurface:
				if (entity instanceof TileEntityWorkSurface){
					return new ContainerWorkSurface(player.inventory, (TileEntityWorkSurface)entity);
				}
				return null;
			case RegisterBlocks.guiIDTestBlock:
				if (entity instanceof TileEntityTestBlock){
					return new ContainerTestBlock(player.inventory, (TileEntityTestBlock)entity, x, y, z);
				}
				return null;
			case RegisterBlocks.guiIDStructureController:
				if (entity instanceof TileEntityStructureController){
					return new ContainerStructureController(player.inventory, (TileEntityStructureController)entity);
				}
				return null;
			}
		}
		
		
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity entity = world.getTileEntity(x, y, z);
		
		if (entity != null) {
			switch (ID) {
			case RegisterBlocks.guiIDAlabasterOven:
				if (entity instanceof TileEntityAlabasterOven) {
					return new GuiAlabasterOven(player.inventory, (TileEntityAlabasterOven) entity);
				}
				return null;
			case RegisterBlocks.guiIDIngotMasher:
				if (entity instanceof TileEntityIngotMasher){
					return new GuiIngotMasher(player.inventory, (TileEntityIngotMasher)entity);
				}
				return null;
			case RegisterBlocks.guiIDWorkSurface:
				if (entity instanceof TileEntityWorkSurface){
						return new GuiWorkSurface(player.inventory, (TileEntityWorkSurface)entity);
				}
				return null;
			case RegisterBlocks.guiIDTestBlock:
				if (entity instanceof TileEntityTestBlock){
					return new GuiTestBlock(player.inventory, (TileEntityTestBlock)entity, world, x, y, z);
				}
				return null;
			case RegisterBlocks.guiIDStructureController:
				if (entity instanceof TileEntityStructureController){
					return new GuiTest(player.inventory, (TileEntityStructureController)entity);
				}
				return null;
			}
		}
		
		
		return null;
	}

}
