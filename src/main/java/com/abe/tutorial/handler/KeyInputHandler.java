package com.abe.tutorial.handler;

import com.abe.tutorial.KeyBindings;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;

//optional class (so far all of the effects for when these keys are pressed are handled in the sword class)
public class KeyInputHandler {

    @SubscribeEvent
    public void onKeyInput(InputEvent.KeyInputEvent event) {
        if(KeyBindings.key_explode.isPressed())
            System.out.println("ping");
        if(KeyBindings.pong.isPressed())
            System.out.println("pong");
    }

}
