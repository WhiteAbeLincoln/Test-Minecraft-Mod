package com.abe.tutorial.tileEntity;

import com.abe.tutorial.blocks.RegisterBlocks;
import com.abe.tutorial.tileEntity.base.MBController;
import com.abe.tutorial.util.externalApi.dimensionalPattern.DimensionalPattern;
import com.abe.tutorial.util.externalApi.dimensionalPattern.DimensionalPattern.BlockState;
import com.abe.tutorial.util.externalApi.dimensionalPattern.DimensionalPattern.Layer;
import com.abe.tutorial.util.externalApi.dimensionalPattern.DimensionalPattern.Row;

import net.minecraft.tileentity.TileEntity;

public class TileTutorial extends MBController{
	
	public TileTutorial(){
		Row row1 = DimensionalPattern.createRow("#");
		Row row2 = DimensionalPattern.createRow("@");
		
		Layer layer1 = DimensionalPattern.createLayer(row1);
		Layer layer2 = DimensionalPattern.createLayer(row2);
		
		BlockState ironFrameState = DimensionalPattern.createBlockState('#', RegisterBlocks.blockUnstableBlock);
		BlockState tutState = DimensionalPattern.createBlockState('@', RegisterBlocks.blockControlBlock);
		
		dPattern = DimensionalPattern.createPattern("testPillar", layer1, layer1, layer1, layer2, ironFrameState, tutState);  
	}
	
@Override
public boolean scanStructure() {
	if (dPattern.hasFormed(worldObj, xCoord, yCoord - 3, zCoord)){
		return true;
	}
	return false;
}
	
}
