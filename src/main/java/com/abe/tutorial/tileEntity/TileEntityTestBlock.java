package com.abe.tutorial.tileEntity;

import scala.reflect.internal.Trees.If;
import cofh.api.energy.EnergyStorage;
import cofh.api.energy.IEnergyHandler;

import com.abe.tutorial.container.ContainerTestBlock;
import com.abe.tutorial.items.RegisterItems;

import cpw.mods.fml.relauncher.Side;
import mantle.blocks.abstracts.InventoryLogic;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public class TileEntityTestBlock extends InventoryLogic implements ISidedInventory, IEnergyHandler {

	private static int[] slots_top = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};	//input slots
	private static final int[] slots_bottom = new int[]{0};	//output slot
	private static final int[] slots_side = new int[]{26};
	public EnergyStorage storage = new EnergyStorage(3600); 
	
	public TileEntityTestBlock() {
		super(27);
	}

	@Override
	public String getInventoryName() {
		return getDefaultName();
	}
	
	@Override
	public boolean hasCustomInventoryName() {
		return true;	
	};
	
	@Override
	public void openInventory() {}

	@Override
	public void closeInventory() {}

	@Override
	public int[] getAccessibleSlotsFromSide(int side) {
		return side == 0 ? slots_bottom : (side == 1 ? slots_top : slots_side);
	}

	@Override
	public ItemStack decrStackSize(int slot, int quantity) {
		if (slot == 0)
        {
            for (int i = 1; i < getSizeInventory(); i++)
                decrStackSize(i, 1);
        }
        return super.decrStackSize(slot, quantity);
	}
	
	@Override
	public boolean canInsertItem(int i, ItemStack itemStack, int j) {
		return (i != 0);
	}

	@Override
	public boolean canExtractItem(int i, ItemStack itemsStack, int j) {
		return i==0;
	}

	@Override
	public Container getGuiContainer(InventoryPlayer inventoryplayer,
			World world, int x, int y, int z) {
		return new ContainerTestBlock(inventoryplayer, this, x, y, z);
	}

	@Override
	protected String getDefaultName() {
		return "crafters.TestBlock";
	}

	
	@Override
	public boolean canDropInventorySlot(int slot) {
		if (slot == 0) return false;
		return true;
	}
	
	@Override
	public boolean canUpdate() {
		return false;
	}

	@Override
	public boolean canInterface(ForgeDirection direction) {
		return direction == ForgeDirection.WEST ? true : false;
	}

	@Override
	public int extractEnergy(ForgeDirection arg0, int maxExtract, boolean simulate) {
		return storage.extractEnergy(maxExtract, simulate);
	}

	@Override
	public int getEnergyStored(ForgeDirection arg0) {
		return storage.getEnergyStored();
	}

	@Override
	public int getMaxEnergyStored(ForgeDirection arg0) {
		return arg0 == ForgeDirection.WEST ? storage.getMaxEnergyStored() : 0;
	}

	@Override
	public int receiveEnergy(ForgeDirection arg0, int maxReceive, boolean simulate) {

		return arg0 == ForgeDirection.WEST ? storage.receiveEnergy(maxReceive, simulate) : 0;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound tags) {

		super.readFromNBT(tags);
		storage.readFromNBT(tags);
	}

	@Override
	public void writeToNBT(NBTTagCompound tags) {

		super.writeToNBT(tags);
		storage.writeToNBT(tags);
	}
}
