package com.abe.tutorial.tileEntity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.abe.tutorial.blocks.RegisterBlocks;

import cpw.mods.fml.relauncher.Side;
import mantle.common.network.PacketUpdateTE;
import mantle.world.CoordTuple;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagIntArray;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public class TileEntityStructureController extends TileEntity implements ISidedInventory {
	
	private ItemStack[] slots = new ItemStack[1];
	private static final int[] slots_bottom = new int[] {0};
	
	int ticks = 0;
	int seconds = 0;
	int minecraftDay = 24000;
	int scanAmt = 4;
	int scanHeight = 12;
	boolean buttonClicked = false;
	boolean firstScan=true;
	int originalCoalCount=0;
	int originalBlockCount=0;
	HashSet<CoordTuple> coalBlockList = new HashSet<CoordTuple>();
	HashSet<CoordTuple> originalBlockList = new HashSet<CoordTuple>();
	HashSet<CoordTuple> changedBlockList = new HashSet<CoordTuple>();
	private String localizedName;
	
	@Override
	public void updateEntity() {
		if (!worldObj.isRemote){
			ticks++;
			minecraftDay--;
			
			if (ticks == 1) scanOnce(worldObj, xCoord, yCoord, zCoord);
			
			if (minecraftDay == 0) 
			
			if (ticks % 20 == 0){
				seconds++;
				System.out.println("Ticking Worked: " + ticks/20);
				
				//TODO run initial on button click (not on tick)
				for (CoordTuple coordTuple : coalBlockList) {
					System.out.println("Coal Block at Coords: " + coordTuple.x +", "+coordTuple.y+", "+coordTuple.z);
				}
				System.out.println("---------------------------------------");
				System.out.println(coalBlockList.size());
			}
			
			if (buttonClicked){
				System.out.println("WORKED: BUTTON CLICKED");
			}
		}
	}
	
	@Override
	public boolean canUpdate() {
		return true;
	}
	
	public void scanOnce(World world, int x, int y, int z){
		for (int x2=0; x2 < scanAmt; x2++){
			for (int z2 = 0; z2 < scanAmt; z2++){
				for (int y2 = 0; y2 < scanHeight; y2++){
				
					for (int x3=0; x3 < scanAmt; x3++){
						for (int z3 = 0; z3 < scanAmt; z3++){
								if (!world.getBlock(x+x2-x3, y+y2, z+z2-z3).equals(RegisterBlocks.blockStructureController)){
									//world.setBlock(x+x2-x3, y+y2, z+z2-z3, RegisterBlocks.blockUnstableBlock);
								}
								
								//gets the coal blocks
								if (world.getBlock(x+x2-x3, y+y2, z+z2-z3).equals(Blocks.coal_block)){
									CoordTuple cooridnates = new CoordTuple(x+x2-x3, y+y2, z+z2-z3);
									
									if (compareCoords(cooridnates, coalBlockList)){
										coalBlockList.add(cooridnates);
									}
									
								}
								
								//TODO Make this set by the structure type
								
								//gets the original block
								if (world.getBlock(x+x2-x3, y+y2, z+z2-z3).equals(Blocks.sand)){
									CoordTuple cooridnates = new CoordTuple(x+x2-x3, y+y2, z+z2-z3);
									originalBlockList.add(cooridnates);
								}
						}
					}
				
				
				}
			}
		}
		
		if (firstScan) originalCoalCount = originalBlockList.size();
		firstScan = false;
	}
	
	public boolean compareCoords(CoordTuple object, HashSet<CoordTuple> coordTuples){
		boolean add = true;
		if (coalBlockList.size() > 0){
			for (CoordTuple cTuple : coalBlockList) {
				if (cTuple.compareTo(object) == 0){
					add = false;
					return add;
				}
			}
		}
		
		return add;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		
		nbt.setInteger("CookProgress", minecraftDay);
		nbt.setBoolean("FirstScan", firstScan);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		
		minecraftDay = nbt.getInteger("CookProgress");
		firstScan = nbt.getBoolean("FirstScan");
		buttonClicked = nbt.getBoolean("ClickButton");
	}

	public void setGuiDisplayName(String displayName) {
		this.localizedName = displayName;
	}
	
	@Override
	public int getSizeInventory() {
		return this.slots.length;
	}

	@Override
	public ItemStack getStackInSlot(int var1) {
		return this.slots[var1];
	}

	@Override
	public ItemStack decrStackSize(int var1, int var2) {
		if (this.slots[var1] != null) {
			ItemStack itemStack;

			if (this.slots[var1].stackSize <= var2) { // when you pull the entire stack out of the inventory
				itemStack = this.slots[var1];
				this.slots[var1] = null;
				return itemStack;
			} else {
				// splits the stack (when stack is rightclicked)
				itemStack = this.slots[var1].splitStack(var2);

				// if the stack cant be split (only 1 item)
				if (this.slots[var1].stackSize == 0) {
					this.slots[var1] = null;
				}

				return itemStack;
			}
		} else {
			return null;
		}
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
		if (this.slots[i] != null) {
			ItemStack itemStack = this.slots[i];
			this.slots[i] = null;
			return itemStack;
		}
		return null;
	}

	@Override
	public void setInventorySlotContents(int i, ItemStack itemStack) {
		this.slots[i] = itemStack;

		if (itemStack != null && itemStack.stackSize > this.getInventoryStackLimit()) {
			itemStack.stackSize = this.getInventoryStackLimit();
		}		
	}

	@Override
	public String getInventoryName() {
		return this.hasCustomInventoryName() ? this.localizedName : "container.structureController";
	}

	@Override
	public boolean hasCustomInventoryName() {
		return this.localizedName != null && this.localizedName.length() > 0;
	}

	@Override
	public int getInventoryStackLimit() {
		// TODO Auto-generated method stub
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return this.worldObj.getTileEntity(this.xCoord, this.yCoord,
				this.zCoord) != this ? false : player.getDistanceSq(
				(double) this.xCoord + 0.5D, (double) this.yCoord + 0.5D,
				(double) this.zCoord + 0.5D) <= 64.0D;
	}

	@Override
	public void openInventory() {}

	@Override
	public void closeInventory() {}

	@Override
	public boolean isItemValidForSlot(int var1, ItemStack var2) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int var1) {
		//TODO check what happens when not pulled from side 0
		return var1 == 0 ? slots_bottom : new int[]{(Integer) null};
	}

	@Override
	public boolean canInsertItem(int slot, ItemStack itemStack, int side) {
		return this.isItemValidForSlot(slot, itemStack);
	}

	@Override
	public boolean canExtractItem(int slot, ItemStack itemStack, int side) {
		return side != 0;
	}
	
	// Client Server Sync
	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt) {
		NBTTagCompound tagCom = pkt.func_148857_g();
		this.readFromNBT(tagCom);
	}

	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound tagCom = new NBTTagCompound();
		this.writeToNBT(tagCom);
		return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord,
		this.blockMetadata, tagCom);
	}
	
}
