package com.abe.tutorial.tileEntity.base;

import net.minecraft.tileentity.TileEntity;

public class MBControlled extends TileEntity{
 
	public MBController mbController;
	
	public MBController getController(){
		return mbController;
	}
	
	public void setController(MBController controller){
		mbController = controller;
	}
	
	public boolean hasController(){
		if (mbController != null){
			return true;
		}
		return false;
	}
}
