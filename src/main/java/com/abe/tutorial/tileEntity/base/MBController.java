package com.abe.tutorial.tileEntity.base;

import com.abe.tutorial.util.externalApi.dimensionalPattern.DimensionalPattern;

import net.minecraft.tileentity.TileEntity;

public class MBController extends TileEntity{
	protected DimensionalPattern dPattern;
	
	public boolean scanStructure(){
		if (dPattern.hasFormed(worldObj, xCoord, yCoord, zCoord)){
			return true;
		}
		return false;
	}
	
}