package com.abe.tutorial.tileEntity;

import com.abe.tutorial.help.Reference;
import com.abe.tutorial.help.RegisterHelper;

import cpw.mods.fml.common.Mod.Instance;

public class RegisterTileEntities {
	
	public static void init() {
		RegisterHelper.registerTileEntity(TileEntityAlabasterOven.class);
		RegisterHelper.registerTileEntity(TileEntityIngotMasher.class);
		RegisterHelper.registerTileEntity(TileEntityWorkSurface.class);
		RegisterHelper.registerTileEntity(TileEntityWindmill.class);
		RegisterHelper.registerTileEntity(TileEntityWindmillBase.class);
		RegisterHelper.registerTileEntity(TileEntityTestBlock.class);
		RegisterHelper.registerTileEntity(TileTutorial.class);
		RegisterHelper.registerTileEntity(TileEntityStructureController.class);
		RegisterHelper.registerTileEntity(TileEntityObsidianTable.class);
	}
	
}
