package com.abe.tutorial.tileEntity;

import com.abe.tutorial.blocks.RegisterBlocks;
import com.abe.tutorial.items.RegisterItems;
import com.abe.tutorial.util.externalApi.dimensionalPattern.DimensionalPattern;
import com.abe.tutorial.util.externalApi.dimensionalPattern.DimensionalPattern.BlockState;
import com.abe.tutorial.util.externalApi.dimensionalPattern.DimensionalPattern.Layer;
import com.abe.tutorial.util.externalApi.dimensionalPattern.DimensionalPattern.Row;
import com.sun.org.apache.bcel.internal.generic.RETURN;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;

public class TileEntityWorkSurface extends TileEntity implements ISidedInventory{

	private ItemStack slots[];
	private String customName;
	
	private static int[] slots_top = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};	//input slots
	private static final int[] slots_bottom = new int[]{0};	//output slot
	private static final int[] slots_side = new int[]{26};
	
	DimensionalPattern dPattern;
	
	public TileEntityWorkSurface(){
		slots = new ItemStack[27+36];
		
		
		Row row1 = DimensionalPattern.createRow("#");
		Row row2 = DimensionalPattern.createRow("@");
		
		Layer layer1 = DimensionalPattern.createLayer(row1);
		Layer layer2 = DimensionalPattern.createLayer(row2);
		
		BlockState ironFrameState = DimensionalPattern.createBlockState('#', RegisterBlocks.blockUnstableBlock);
		BlockState tutState = DimensionalPattern.createBlockState('@', RegisterBlocks.blockWorkSurface);
		
		dPattern = DimensionalPattern.createPattern("pillar", layer1, layer1, layer1, layer2, ironFrameState, tutState);  
		
	}
	
	@Override
	public int getSizeInventory() {
		return slots.length;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return slots[i];
	}
	
	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
		if (slots[i] != null){
			ItemStack itemStack = slots[i];
			slots[i] = null;
			return itemStack;
		} else {
			return null;
		}
	}
	
	@Override
	public void setInventorySlotContents(int i, ItemStack itemStack) {
		slots[i] = itemStack;
		if (itemStack != null && itemStack.stackSize > getInventoryStackLimit()) {
			itemStack.stackSize = getInventoryStackLimit();
		}
	}
	
	@Override
	public int[] getAccessibleSlotsFromSide(int side) {
		return side == 0 ? slots_bottom : (side == 1 ? slots_top : slots_side);
	}
	
	public boolean scanBelow(){
		if (dPattern.hasFormed(worldObj, xCoord, yCoord - 3, zCoord)){
			worldObj.setBlockToAir(xCoord, yCoord+1, zCoord);
			System.out.println("GOODLKJLAKJ");
			return true;
		}
		return false;
	}
	
	@Override
	public String getInventoryName() {
		return this.hasCustomInventoryName() ? this.customName : "container.workSurface";
	}

	@Override
	public boolean hasCustomInventoryName() {
		return this.customName != null && this.customName.length() > 0;
	}
	
	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		if (worldObj.getTileEntity(xCoord, yCoord, zCoord) != this) return false;
		else {
			return player.getDistanceSq((double)xCoord + 0.5D, (double)yCoord+ 0.5D, (double)zCoord+ 0.5D) <= 64;
		}
	}

	public void openInventory() {}
	public void closeInventory() {}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack itemStack) {
		return slot == 0 ? false : (slot == 1 ? itemStack.getItem() == RegisterItems.itemBattery : (slot >= 2 && slot <= 27 ? true : false));
	}
	
	@Override
	// called when stack is pulled from inventory
	public ItemStack decrStackSize(int i, int j) {
		if (this.slots[i] != null) {
			ItemStack itemStack;

			if (this.slots[i].stackSize <= j) { // when you pull the entire stack out of the inventory
				itemStack = this.slots[i];
				this.slots[i] = null;
				return itemStack;
			} else {
				// splits the stack (when stack is rightclicked)
				itemStack = this.slots[i].splitStack(j);

				// if the stack cant be split (only 1 item)
				if (this.slots[i].stackSize == 0) {
					this.slots[i] = null;
				}

				return itemStack;
			}
		} else {
			return null;
		}
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		NBTTagList list = new NBTTagList();
		
		for (int i=0; i< slots.length; i++){
			if (slots[i] != null){
				NBTTagCompound nbt1 = new NBTTagCompound();
				nbt1.setByte("Slot", (byte)i);
				this.slots[i].writeToNBT(nbt1);
				list.appendTag(nbt1);
			}
		}
		
		nbt.setTag("Items", list);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		NBTTagList list = nbt.getTagList("Items", 10);
		slots = new ItemStack[getSizeInventory()];
		
		for (int i=0; i < list.tagCount(); i++){
			NBTTagCompound nbt1 = (NBTTagCompound)list.getCompoundTagAt(i);
			byte b0 = nbt1.getByte("Slot");
			
			if (b0 >= 0 && b0 < slots.length){
				slots[b0] = ItemStack.loadItemStackFromNBT(nbt1);
			}
		}
		
	}

	@Override
	public boolean canInsertItem(int slot, ItemStack itemStack, int side) {
		return this.isItemValidForSlot(slot, itemStack);
	}

	@Override
	public boolean canExtractItem(int slot, ItemStack itemStack, int side) {
		return slot == 0;
	}

}
