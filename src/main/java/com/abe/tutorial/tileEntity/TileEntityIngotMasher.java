package com.abe.tutorial.tileEntity;

import scala.reflect.internal.Trees.If;

import com.abe.tutorial.blocks.IngotMasher;
import com.abe.tutorial.crafting.MashingRecipes;
import com.abe.tutorial.items.RegisterItems;
import com.sun.org.apache.xml.internal.security.keys.content.RetrievalMethod;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;

public class TileEntityIngotMasher extends TileEntity implements ISidedInventory{

	private ItemStack slots[];
	private String customName;
	public int dualCookTime;
	public int dualPower;
	public static final int maxPower=10000;
	public static final int mashingSpeed=100;
	
	private static final int[] slots_top = new int[]{0, 1};
	private static final int[] slots_bottom = new int[]{3};
	private static final int[] slots_side = new int[]{2};
	
	public TileEntityIngotMasher(){
		slots = new ItemStack[4];
	}
	
	@Override
	public int getSizeInventory() {
		return slots.length;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return slots[i];
	}


	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
		if (slots[i] != null){
			ItemStack itemStack = slots[i];
			slots[i] = null;
			return itemStack;
		} else {
			return null;
		}
	}

	@Override
	public void setInventorySlotContents(int i, ItemStack itemStack) {
		slots[i] = itemStack;
		if (itemStack != null && itemStack.stackSize > getInventoryStackLimit()) {
			itemStack.stackSize = getInventoryStackLimit();
		}
	}
	
	@Override
	public int[] getAccessibleSlotsFromSide(int side) {
		return side == 0 ? slots_bottom : (side == 1 ? slots_top : slots_side);
	}

	@Override
	public String getInventoryName() {
		return this.hasCustomInventoryName() ? this.customName : "container.ingotMasher";
	}

	@Override
	public boolean hasCustomInventoryName() {
		return this.customName != null && this.customName.length() > 0;
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		if (worldObj.getTileEntity(xCoord, yCoord, zCoord) != this) return false;
		else {
			return player.getDistanceSq((double)xCoord + 0.5D, (double)yCoord+ 0.5D, (double)zCoord+ 0.5D) <= 64;
		}
	}

	public void openInventory() {}
	public void closeInventory() {}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack itemStack) {
		return slot == 3 /*(outputSlot)*/ ? false : (slot == 2  /*(powerSlot)*/ ? hasItemPower(itemStack) : true);
	}

	public boolean hasItemPower(ItemStack itemStack) {
		return getItemPower(itemStack) > 0;
	}

	private static int getItemPower(ItemStack itemStack) {
		if (itemStack == null){
			return 0;
		} else {
			Item item = itemStack.getItem();
			
			if (item == RegisterItems.itemTreePitch) return 50;
			
			return 0;
		}
	}

	@Override
	// called when stack is pulled from inventory
	public ItemStack decrStackSize(int i, int j) {
		if (this.slots[i] != null) {
			ItemStack itemStack;

			if (this.slots[i].stackSize <= j) { // when you pull the entire stack out of the inventory
				itemStack = this.slots[i];
				this.slots[i] = null;
				return itemStack;
			} else {
				// splits the stack (when stack is rightclicked)
				itemStack = this.slots[i].splitStack(j);

				// if the stack cant be split (only 1 item)
				if (this.slots[i].stackSize == 0) {
					this.slots[i] = null;
				}

				return itemStack;
			}
		} else {
			return null;
		}
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setShort("PowerTime", (short)dualPower);
		nbt.setShort("CookTime", (short)dualCookTime);
		NBTTagList list = new NBTTagList();
		
		for (int i = 0; i < slots.length; i++){
			if (slots[i] != null){
				NBTTagCompound nbt1 = new NBTTagCompound();
				nbt1.setByte("Slot", (byte)i);	//stores slot number
				this.slots[i].writeToNBT(nbt1);
				list.appendTag(nbt1);
			}
		}
		nbt.setTag("Items", list);	//adds the list to storage
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		NBTTagList list = nbt.getTagList("Items", 10);
		slots = new ItemStack[getSizeInventory()];
		
		for (int i=0; i < list.tagCount(); i++){
			NBTTagCompound nbt1 = (NBTTagCompound)list.getCompoundTagAt(i);
			byte b0 = nbt1.getByte("Slot");
			
			if (b0 >= 0 && b0 < slots.length){
				slots[b0] = ItemStack.loadItemStackFromNBT(nbt1);
			}
		}
		
		dualPower = nbt.getShort("PowerTime");
		dualCookTime = nbt.getShort("CookTime");
	}

	@Override
	public boolean canInsertItem(int slot, ItemStack itemStack, int side) {
		return this.isItemValidForSlot(slot, itemStack);
	}

	@Override
	public boolean canExtractItem(int slot, ItemStack itemStack, int side) {
		return side != 0 || slot != 1 || itemStack.getItem() == Items.bucket;
	}
	
	public int getPowerRemainingScaled(int i){
		return (dualPower * i)/maxPower;
	}
	
	public int getMasherProgressScaled(int i){
		return (dualCookTime * i) / this.mashingSpeed;
	}
	
	public boolean canMash(){
		if (slots[0] == null || slots[1] == null){
			return false;  // if the input slots have nothing
		} else {
			ItemStack itemstack = MashingRecipes.smelting().getSmeltingResult(this.slots[0], this.slots[1]);
            if (itemstack == null) return false;
            if (this.slots[3] == null) return true;
            if (!this.slots[3].isItemEqual(itemstack)) return false;
            int result = slots[3].stackSize + itemstack.stackSize;
            return result <= getInventoryStackLimit() && result <= this.slots[3].getMaxStackSize();
		}
	}
	
	private void mashItem(){
		if (canMash()){
			worldObj.playSoundEffect((double)xCoord + 0.5D, (double)yCoord + 0.5D, (double)zCoord + 0.5D, "random.anvil_use", 1.0F, 0.6F-worldObj.rand.nextFloat() * 0.1F);
			ItemStack itemStack = MashingRecipes.smelting().getSmeltingResult(this.slots[0], this.slots[1]);
			
			if (slots[3] == null){
				slots[3] = itemStack.copy();
			} else if (slots[3].isItemEqual(itemStack)){
				slots[3].stackSize += itemStack.stackSize;
			}
			
			for (int i=0; i<2; i++){
				if (slots[i].stackSize <= 0){
					slots[i] = new ItemStack(slots[i].getItem().setFull3D());
				}else {
					slots[i].stackSize--;
				}
				
				if (slots[i].stackSize <= 0){
					slots[i] = null;
				}
			}
		}
	}
	
	public boolean hasPower() {
		return dualPower > 0;
	}
	
	public boolean isMashing() {
		return this.dualCookTime > 0;
	}
	
	public void updateEntity(){
		boolean flag = this.hasPower();
		boolean flag1 = false;
		
		if (hasPower() && this.isMashing()){
			this.dualPower--;
		}
		
		if (!worldObj.isRemote){
			if (this.hasItemPower(this.slots[2]) && this.dualPower <= (this.maxPower - this.getItemPower(this.slots[2]))){
				this.dualPower+= getItemPower(this.slots[2]);
				
				if (this.slots[2] != null){
					flag1 = true;
					this.slots[2].stackSize--;
					
					if (this.slots[2].stackSize==0){
						this.slots[2] = this.slots[2].getItem().getContainerItem(this.slots[2]);
					}
				}
			}
			
			if (hasPower() && canMash()){
				dualCookTime++;
				if (this.dualCookTime == this.mashingSpeed){
					this.dualCookTime = 0;
					this.mashItem();
					flag1=true;
				}
			} else{
				dualCookTime = 0;
			}
			
			if (flag != this.isMashing()){
				flag1=true;
				IngotMasher.updateBlockState(this.isMashing(), this.worldObj, this.xCoord, this.yCoord, this.zCoord);
			}
		}
		
		if (flag1){
			this.markDirty();
		}
	}
}
