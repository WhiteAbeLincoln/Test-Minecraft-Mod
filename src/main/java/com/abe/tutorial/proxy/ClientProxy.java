package com.abe.tutorial.proxy;

import com.abe.tutorial.blocks.RegisterBlocks;
import com.abe.tutorial.model.renderer.ItemRenderObsidianTable;
import com.abe.tutorial.model.renderer.RenderObsidianTable;
import com.abe.tutorial.model.renderer.RenderWindmill;
import com.abe.tutorial.model.renderer.RenderWindmillBase;
import com.abe.tutorial.tileEntity.TileEntityObsidianTable;
import com.abe.tutorial.tileEntity.TileEntityWindmill;
import com.abe.tutorial.tileEntity.TileEntityWindmillBase;

import cpw.mods.fml.client.registry.ClientRegistry;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.Item;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;


public class ClientProxy extends CommonProxy {

	@Override
    public void registerRenderers() {
            // This is for rendering entities and so forth
		
		//obsidianTable
		TileEntitySpecialRenderer obsidianTableRenderer = new RenderObsidianTable();
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityObsidianTable.class, obsidianTableRenderer);
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(RegisterBlocks.blockObsidianTable), new ItemRenderObsidianTable(obsidianTableRenderer, new TileEntityObsidianTable()));
		
		//windmill
		TileEntitySpecialRenderer windmillRenderer = new RenderWindmill();
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWindmill.class, windmillRenderer);
		
		//windmill base
		TileEntitySpecialRenderer windmillBaseRenderer = new RenderWindmillBase();
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWindmillBase.class, windmillBaseRenderer);
    }
	
	public void registerTileEntitySpecialRenderer(){
		
	}
}
