package com.abe.tutorial;


import com.abe.tutorial.blocks.RegisterBlocks;
import com.abe.tutorial.items.RegisterItems;

import net.minecraft.stats.Achievement;
import net.minecraftforge.common.AchievementPage;

public class RegisterAchievements {

	public static AchievementPage modPage1;
	public static Achievement smeltTutorialIngotAchievement;
	public static Achievement mineTutorialOreAchievement;
	
	public static void init() {
		
		
		
		//new achievement... params are - (unlocalizedName, referenceName, x, y, Icon(derived from item or block), required achievement)
		mineTutorialOreAchievement = (new Achievement("achievement.mineTutOre", "mineTutOre", 0, 0, RegisterBlocks.oreUnstableOre, (Achievement)null)).initIndependentStat().registerStat();
		smeltTutorialIngotAchievement = (new Achievement("achievement.smeltTutIngot", "smeltTutIngot", 2, 1, RegisterItems.itemUnstableIngot, mineTutorialOreAchievement)).registerStat();
		
		
		//creates achievement page
		//params are... (Name, array of achievements)
		modPage1 = new AchievementPage("Tutorial Achievements", new Achievement[]{mineTutorialOreAchievement, smeltTutorialIngotAchievement});
		modPage1.registerAchievementPage(modPage1);
	}
}
