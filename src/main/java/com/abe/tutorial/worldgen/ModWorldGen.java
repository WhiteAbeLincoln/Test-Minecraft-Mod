package com.abe.tutorial.worldgen;

import java.util.Random;

import scala.inline;

import com.abe.tutorial.blocks.RegisterBlocks;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import cpw.mods.fml.common.IWorldGenerator;

public class ModWorldGen implements IWorldGenerator{

	
	
	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world,
			IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		
		//0 surface, -1 nether, 1 end
		switch (world.provider.dimensionId) {
		case 0:
			//Generate our surface world
			generateSurface(world, random, chunkX*16, chunkZ*16);
		case -1:
			//Generate our nether world
			generateNether(world, random, chunkX*16, chunkZ*16);
		case 1:
			//Generate our end world
			generateEnd(world, random, chunkX*16, chunkZ*16);

		default:
			break;
		}
		
	}

	private void generateSurface(World world, Random random, int x, int z) {
		//this.addOreSpawn(ore, world, random, block xpos, block zpos, maxX, maxZ, maxVeinSize, chance to spawn, minY, maxY)
		
		//Block waterBlock = Blocks.water;
		
		this.addOreSpawn(RegisterBlocks.oreUnstableOre, world, random, x, z, 16, 16, 4+random.nextInt(5), 15, 0, 31);
		this.addOreSpawn(RegisterBlocks.oreTinOreBlock, world, random, x, z, 16, 16, 4+random.nextInt(6), 25, 38, 100);
		this.addOreSpawn(RegisterBlocks.oreZincOreBlock, world, random, x, z, 16, 16, 4+random.nextInt(6), 25, 38, 100);
		this.addOreSpawn(RegisterBlocks.oreNickelOreBlock, world, random, x, z, 16, 16, 4+random.nextInt(6), 25, 38, 100);
		this.addOreSpawn(RegisterBlocks.oreRhodiumOreBlock, world, random, x, z, 16, 16, 4+random.nextInt(6), 25, 38, 100);
		this.addOreSpawn(RegisterBlocks.oreVanadiumOreBlock, world, random, x, z, 16, 16, 4+random.nextInt(6), 25, 38, 100);
		this.addOreSpawn(RegisterBlocks.oreManganeseOreBlock, world, random, x, z, 16, 16, 4+random.nextInt(6), 25, 38, 100);
		this.addOreSpawn(RegisterBlocks.oreTopazOre, world, random, x, z, 16, 16, 4+random.nextInt(4), 20, 20, 58);
		this.addAdvancedOreSpawn(RegisterBlocks.oreAlabasterBlock, Blocks.sand, Blocks.water, "Desert", world, random, x, z, 16, 16, 10+random.nextInt(6), 35, 62, 100);
		
		
	}
	

	private void generateNether(World world, Random random, int x, int z) {
		this.addOreSpawn(RegisterBlocks.oreNetherZincOreBlock, Blocks.netherrack, world, random, x, z, 16, 16, 4+random.nextInt(6), 25, 38, 100);	
	}

	private void generateEnd(World world, Random random, int x, int z) {
		// TODO Auto-generated method stub
		
	}
	
	private void addOreSpawn(Block block, World world, Random random, int blockXPos, int blockZPos, int maxX, int maxZ, int maxVeinSize, int chanceToSpawn, int minY, int maxY) {
		
		for (int i = 0; i < chanceToSpawn; i++){
			//specifying block x y and z positions
			int posX = blockXPos + random.nextInt(maxX);
			int posY = minY + random.nextInt(maxY - minY);
			int posZ = blockZPos + random.nextInt(maxZ);
			
			(new WorldGenMinable(block, maxVeinSize)).generate(world, random, posX, posY, posZ);
		}
		
	}
	
	private void addOreSpawn(Block block, Block replaceBlock, World world, Random random, int blockXPos, int blockZPos, int maxX, int maxZ, int maxVeinSize, int chanceToSpawn, int minY, int maxY) {
		
		for (int i = 0; i < chanceToSpawn; i++){
			//specifying block x y and z positions
			int posX = blockXPos + random.nextInt(maxX);
			int posY = minY + random.nextInt(maxY - minY);
			int posZ = blockZPos + random.nextInt(maxZ);
			
			(new WorldGenMinable(block, maxVeinSize, replaceBlock)).generate(world, random, posX, posY, posZ);
		}
		
	}
	
	private void addAdvancedOreSpawn(Block block, Block replaceBlock, Block nearBlock, String biome, World world, Random random, int blockXPos, int blockZPos, int maxX, int maxZ, int maxVeinSize, int chanceToSpawn, int minY, int maxY) {
		
		for (int i = 0; i < chanceToSpawn; i++){
			//specifying block x y and z positions
			int posX = blockXPos + random.nextInt(maxX);
			int posY = minY + random.nextInt(maxY - minY);
			int posZ = blockZPos + random.nextInt(maxZ);
			
			BiomeGenBase biomeGenBase = world.getWorldChunkManager().getBiomeGenAt(posX, posZ);
			int l = random.nextInt(maxVeinSize - 2) + 2;
			byte b0 = 1;
			
			
			if (biomeGenBase.biomeName.contains(biome)){
				for (int i1 = posX - l; i1 <= posX + l; ++i1)
	            {
	                for (int j1 = posZ - l; j1 <= posZ + l; ++j1)
	                {
	                    int k1 = i1 - posX;
	                    int l1 = j1 - posZ;

	                    if (k1 * k1 + l1 * l1 <= l * l)
	                    {
	                        for (int i2 = posY - b0; i2 <= posY + b0; ++i2)
	                        {
	                            Block block1 = world.getBlock(i1, i2, j1);

	                            if (block1 == nearBlock || block1 == block)
	                            {
	                            	(new WorldGenMinable(block, maxVeinSize, replaceBlock)).generate(world, random, posX, posY, posZ);
	                            }
	                        }
	                    }
	                }
	            }
				
			}
		}
		
	}

}
