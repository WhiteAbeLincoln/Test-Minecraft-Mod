package com.abe.tutorial.worldgen;

import cpw.mods.fml.common.registry.GameRegistry;

public class RegisterWorldGen {
	
	static ModWorldGen eventWorldGen = new ModWorldGen();
	
	public static void init(){
		//registers the worldGenerator
		GameRegistry.registerWorldGenerator(eventWorldGen, 0);
	}

}
