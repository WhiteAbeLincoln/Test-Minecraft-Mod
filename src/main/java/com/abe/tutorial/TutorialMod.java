package com.abe.tutorial;

import java.util.EnumMap;
import java.util.logging.Level;

import scala.reflect.internal.Trees.This;

import com.abe.tutorial.blocks.BlockUnstableOre;
import com.abe.tutorial.blocks.RegisterBlocks;
import com.abe.tutorial.core.network.PacketHandler;
import com.abe.tutorial.core.network.PacketUpdate;
import com.abe.tutorial.core.network.TutorialChannelHandler;
import com.abe.tutorial.handler.CraftingHandler;
import com.abe.tutorial.handler.FuelHandler;
import com.abe.tutorial.handler.GuiHandler;
import com.abe.tutorial.handler.KeyInputHandler;
import com.abe.tutorial.handler.RecipesHandler;
import com.abe.tutorial.handler.TutorialEventHandler;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.help.RegisterHelper;
import com.abe.tutorial.items.ItemUnstableSwordEnhanced;
import com.abe.tutorial.items.RegisterItems;
import com.abe.tutorial.proxy.CommonProxy;
import com.abe.tutorial.tileEntity.RegisterTileEntities;
import com.abe.tutorial.worldgen.RegisterWorldGen;

import mantle.common.network.AbstractPacket;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.EnumHelper;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.FMLEmbeddedChannel;
import cpw.mods.fml.common.network.FMLOutboundHandler;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.FMLOutboundHandler.OutboundTarget;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;

@Mod(modid = Reference.MODID, version = Reference.VERSION, name = Reference.NAME_STRING)

public class TutorialMod
{
	
	TutorialEventHandler events = new TutorialEventHandler();
	KeyInputHandler keys = new KeyInputHandler();
	
	
    @Instance(Reference.MODID)
    public static TutorialMod instance;
    
    @SidedProxy(clientSide="com.abe.tutorial.proxy.ClientProxy", serverSide="com.abe.tutorial.proxy.CommonProxy")
    public static CommonProxy proxy;
    
    @EventHandler
    public void preinit(FMLPreInitializationEvent event){
    	
    	TutLogger.initLog();
    	
    	TutorialChannelHandler.registerHandler(Reference.NET_CHANNEL_NAME, PacketUpdate.class);
    	
    	MinecraftForge.EVENT_BUS.register(events);
    	FMLCommonHandler.instance().bus().register(events);
    	FMLCommonHandler.instance().bus().register(keys);
    	FMLCommonHandler.instance().bus().register(new CraftingHandler());
    	System.out.println("TutorialMod: Events Loaded");
    	    	
    	//creativetabs
    	RegisterCreativeTabs.init();
		System.out.println("TutorialMod: All CreativeTabs Loaded");
		
		//items
		RegisterItems.loadItems();
		System.out.println("TutorialMod: All Items Loaded");
		
		//blocks
		RegisterBlocks.loadBlocks();
		System.out.println("TutorialMod: All Blocks Loaded");
		
		RegisterTileEntities.init();
		
		//spawn
		RegisterWorldGen.init();
		
		//renderers
		proxy.registerRenderers();
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {    	
    	KeyBindings.init();
    	RegisterAchievements.init();
    	
    	NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());
    	GameRegistry.registerFuelHandler(new FuelHandler());
    	
    	RecipesHandler.addRecipes();
    	RecipesHandler.addSmelting();
    	RecipesHandler.addMashing();
    	System.out.println("TutorialMod: All Recipes Loaded");
    }
    
}