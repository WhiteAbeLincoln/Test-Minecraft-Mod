package com.abe.tutorial.util;

import java.util.Random;

import com.abe.tutorial.items.RegisterItems;

import cpw.mods.fml.common.gameevent.PlayerEvent.ItemCraftedEvent;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

public class CreationTools {

	/**
	 * Creates a random explosion, position defined by entity.
	 * @param randomRange 	the range for the random integer (random.nextInt(range))
	 * @param strength 	the strength of the explosion
	 * @param world 	current world
	 * @param entity 	current entity (that caused explosion)
	 */
	public static void createRandomExplosion(int randomRange, float strength, World world, Entity entity){
		Random random = new Random();
		int chance = random.nextInt(randomRange);
		
		if (chance == ((int)randomRange/2) && !world.isRemote){
			world.createExplosion((Entity)null, entity.posX, entity.posY, entity.posZ, strength, true);
			System.out.println("SUCCESS CHANCE");
		}
	}
	
	/**
	 * Creates a random explosion, position defined coordinates.
	 * @param randomRange 	the range for the random integer (random.nextInt(range))
	 * @param strength 	the strength of the explosion
	 * @param world 	current world
	 * @param entity 	current entity (that caused explosion)
	 */
	public static void createRandomExplosion(int randomRange, float strength, World world, int x, int y, int z){
		Random random = new Random();
		int chance = random.nextInt(randomRange);
		
		if (chance == ((int)randomRange/2) && !world.isRemote){
			world.createExplosion((Entity)null, x, y, z, strength, true);
			System.out.println("SUCCESS CHANCE");
		}
	}
	
	/**
	 * Allows for easy implementation of durable items
	 * @param e	the crafting event
	 * @param currentStep	the current step in the for loop that is parsing through crafting table (i)
	 * @param durableItem	the item to damage
	 * @param damageAmount	the amount of damage to inflict on use
	 */
	public static void craftDurableItem(ItemCraftedEvent e, int currentStep, Item durableItem, int damageAmount){
		if (e.craftMatrix.getStackInSlot(currentStep) != null){	//if the stack at i is not null
			ItemStack item0 = e.craftMatrix.getStackInSlot(currentStep);		//gets the stack
			
			if (item0 != null && item0.getItem() == durableItem){		//if desired Item
				ItemStack k = new ItemStack(durableItem, 2, (item0.getItemDamage() + damageAmount));		//sets a new itemstack with 1 more damage
				
				if (k.getItemDamage() >= k.getMaxDamage()){		//if at max damage
					k.stackSize--;		//remove stack
				}
				e.craftMatrix.setInventorySlotContents(currentStep, k);		//sets the stack back into the slot
			}
		}
	}
	
	/**
	 * Creates an explosion that drops all items.
	 * @param exploder	the entity that causes/creates the explosion
	 * @param x	explosion X position
	 * @param y	explosion Y position
	 * @param z	explosion Z position
	 * @param strength	the strength of the explosion
	 * @param flame	spawn flame blocks?
	 * @param smoke	spawn smoke particles?
	 * @return
	 */
    public static Explosion createExplosion(Entity exploder, double x, double y, double z, float strength, boolean flame, boolean smoke)
    {
        return newExplosion(exploder, x, y, z, strength, flame, smoke);
    }

    /**
     * returns a new explosion. Does initiation (at time of writing Explosion is not finished)
     */
    public static Explosion newExplosion(Entity entity, double xPos, double yPos, double zPos, float strength, boolean flame, boolean smoke)
    {
        DropExplosion explosion = new DropExplosion(entity.worldObj, entity, xPos, yPos, zPos, strength);
        explosion.isFlaming = flame;
        explosion.isSmoking = smoke;
        explosion.doExplosionA();
        explosion.doExplosionB(true);
        return explosion;
    }
}
