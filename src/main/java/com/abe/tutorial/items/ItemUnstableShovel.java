package com.abe.tutorial.items;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.util.CreationTools;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;

public class ItemUnstableShovel extends ItemSpade {

	public ItemUnstableShovel(ToolMaterial material) {
		super(material);
		
		setUnlocalizedName("UnstableShovel");
		setCreativeTab(RegisterCreativeTabs.tutorialTabTools);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister){
		this.itemIcon = iconRegister.registerIcon(Reference.MODID + ":" + this.getUnlocalizedName().substring(5));
	}
	
	@Override
	public boolean onBlockStartBreak(ItemStack itemstack, int X, int Y, int Z,
			EntityPlayer player) {
		
		CreationTools.createRandomExplosion(10, 2.0F, player.worldObj, X, Y, Z);
		
		return super.onBlockStartBreak(itemstack, X, Y, Z, player);
	}

}
