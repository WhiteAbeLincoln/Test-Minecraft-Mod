package com.abe.tutorial.items;

import com.abe.tutorial.blocks.BlockMetadataBlock;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemMetadataBlocks extends ItemBlock{

	public final static String[] SUBBLOCKS_STRINGS = new String[]{"limestone", "shale", "granite", "andesite"};
	
	public ItemMetadataBlocks(Block block){
		super(block);
		setHasSubtypes(true);
	}
	
	public String getUnlocalizedName(ItemStack itemstack){
		int i = itemstack.getItemDamage();
		if (i < 0 || i >= BlockMetadataBlock.SUBBLOCKS_STRINGS.length){
			i = 0;
		}
		
		return super.getUnlocalizedName() + "." + BlockMetadataBlock.SUBBLOCKS_STRINGS[i];
	}
	
	public int getMetadata(int meta){
		return meta;
	}
}
