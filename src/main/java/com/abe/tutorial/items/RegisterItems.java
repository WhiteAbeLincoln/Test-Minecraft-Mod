package com.abe.tutorial.items;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraftforge.common.util.EnumHelper;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.blocks.RegisterBlocks;
import com.abe.tutorial.help.RegisterHelper;

import cpw.mods.fml.common.registry.GameRegistry;

public class RegisterItems {
	
	//materials
	public static Item itemTreePitch;
	public static final int itemTreePitchBurnTime = 800;
	//adds a new material for this item
	//(name, harvest level, durability, speed in destroying blocks, damage(in half hearts), enchantability)
	public static final ToolMaterial UNSTABLEINGOT_TOOL_MATERIAL = EnumHelper.addToolMaterial("unstableIngotMaterial", 2, 1300, 10.5F, 4.0F, 10);
	//armor material params: name, base multiplier, array of type multipliers{helmet, chest, leggings, shoes}, enchantability
	public static final ArmorMaterial UNSTABLEINGOT_ARMOR_MATERIAL = EnumHelper.addArmorMaterial("unstableIngotArmorMaterial", 22, new int[]{2,6,5,2}, 10);
	
	//windmill
	public static Item itemWindmill;
	
	//durableItems
	public static Item itemIronHammer;
	public static Item itemIronPunch;
	public static Item itemBattery;
	
	
	//ingots
	public static Item itemUnstableIngot;
	public static Item itemTinIngot;
	public static Item itemZincIngot;
	public static Item itemNickelIngot;
	public static Item itemManganeseIngot;
	public static Item itemVanadiumIngot;
	public static Item itemRhodiumIngot;
	
	//armor
	public static Item tutorialHelmet;
	public static Item tutorialChest;
	public static Item tutorialBoots;
	public static Item tutorialPants;

    //tools
	public static Item itemUnstableSword;
    public static Item itemUnstableSwordEnhanced;
    public static Item itemUnstableAxe;
    public static Item itemUnstableAxeEnhanced;
    public static Item itemUnstableShovel;
    public static Item itemUnstableShovelEnhanced;
    public static Item itemUnstableHoe;
    public static Item itemUnstableHoeEnhanced;
    public static Item itemUnstablePickaxe;
    public static Item itemUnstablePickaxeEnhanced;
    
    //items
    public static Item itemTinCog;
    public static Item itemIronWasher;
    public static Item itemIronDisc;
	public static Item itemSiliconChunk;
	public static Item itemTopaz;
    
	public static void loadItems(){
		
		itemWindmill = new ItemWindmill().setUnlocalizedName("WindmillItem");
		RegisterHelper.registerItem(itemWindmill);
		
		// materials\fuel
		itemTreePitch = new ModItem().setUnlocalizedName("TreePitch");		//setUnlocalizedName sets the name that is used in code (to reference the localized name)
		RegisterHelper.registerItem(itemTreePitch);
		
		itemSiliconChunk = new ModItem().setUnlocalizedName("SiliconChunk");
		RegisterHelper.registerItem(itemSiliconChunk);
		
		itemTopaz = new ModItem().setUnlocalizedName("TopazChunk");
		RegisterHelper.registerItem(itemTopaz);
		
		
		//ingots
		itemUnstableIngot = new ModItem().setUnlocalizedName("tutorialIngotItem");
		RegisterHelper.registerItem(itemUnstableIngot);

		itemTinIngot = new ModItem().setUnlocalizedName("TinIngot");
		RegisterHelper.registerItem(itemTinIngot);
		
		itemZincIngot = new ModItem().setUnlocalizedName("ZincIngot");
		RegisterHelper.registerItem(itemZincIngot);
		
		itemNickelIngot = new ModItem().setUnlocalizedName("NickelIngot");
		RegisterHelper.registerItem(itemNickelIngot);
		
		itemManganeseIngot = new ModItem().setUnlocalizedName("ManganeseIngot");
		RegisterHelper.registerItem(itemManganeseIngot);
		
		itemVanadiumIngot = new ModItem().setUnlocalizedName("VanadiumIngot");
		RegisterHelper.registerItem(itemVanadiumIngot);
		
		itemRhodiumIngot = new ModItem().setUnlocalizedName("RhodiumIngot");
		RegisterHelper.registerItem(itemRhodiumIngot);
		
		
		//items
		itemTinCog = new ModItem().setUnlocalizedName("TinCog");
		RegisterHelper.registerItem(itemTinCog);
		
		itemIronDisc = new ModItem().setUnlocalizedName("IronDisc");
		RegisterHelper.registerItem(itemIronDisc);
		
		itemIronWasher = new ModItem().setUnlocalizedName("IronWasher");
		RegisterHelper.registerItem(itemIronWasher);
		
		
		//durable
		itemIronHammer = new IronHammer().setUnlocalizedName("IronHammer");
		RegisterHelper.registerItem(itemIronHammer);
		
		itemIronPunch = new IronPunch().setUnlocalizedName("IronPunch");
		RegisterHelper.registerItem(itemIronPunch);
		
		itemBattery = new Battery(100).setUnlocalizedName("Battery").setCreativeTab(RegisterCreativeTabs.tutorialTabItems);
		RegisterHelper.registerItem(itemBattery);
		
		//tools
		itemUnstableSwordEnhanced = new ItemUnstableSwordEnhanced(UNSTABLEINGOT_TOOL_MATERIAL);
		RegisterHelper.registerItem(itemUnstableSwordEnhanced);
		
		itemUnstableSword = new ItemUnstableSword(UNSTABLEINGOT_TOOL_MATERIAL);
		RegisterHelper.registerItem(itemUnstableSword);
		
		itemUnstableAxeEnhanced = new ItemUnstableAxeEnhanced(UNSTABLEINGOT_TOOL_MATERIAL);
		RegisterHelper.registerItem(itemUnstableAxeEnhanced);
		
		itemUnstableAxe = new ItemUnstableAxe(UNSTABLEINGOT_TOOL_MATERIAL);
		RegisterHelper.registerItem(itemUnstableAxe);
		
		itemUnstablePickaxeEnhanced = new ItemUnstablePickaxeEnhanced(UNSTABLEINGOT_TOOL_MATERIAL);
		RegisterHelper.registerItem(itemUnstablePickaxeEnhanced);
		
		itemUnstablePickaxe = new ItemUnstablePickaxe(UNSTABLEINGOT_TOOL_MATERIAL);
		RegisterHelper.registerItem(itemUnstablePickaxe);
		
		itemUnstableShovelEnhanced = new ItemUnstableShovelEnhanced(UNSTABLEINGOT_TOOL_MATERIAL);
		RegisterHelper.registerItem(itemUnstableShovelEnhanced);
		
		itemUnstableShovel = new ItemUnstableShovel(UNSTABLEINGOT_TOOL_MATERIAL);
		RegisterHelper.registerItem(itemUnstableShovel);
		

		//armor
		tutorialHelmet = new ItemTutorialArmor(UNSTABLEINGOT_ARMOR_MATERIAL, 0, "tutorialHelmet");
		RegisterHelper.registerItem(tutorialHelmet);
		
		tutorialChest = new ItemTutorialArmor(UNSTABLEINGOT_ARMOR_MATERIAL, 1, "tutorialChest");
		RegisterHelper.registerItem(tutorialChest);
		
		tutorialPants = new ItemTutorialArmor(UNSTABLEINGOT_ARMOR_MATERIAL, 2, "tutorialPants");
		RegisterHelper.registerItem(tutorialPants);
		
		tutorialBoots = new ItemTutorialArmor(UNSTABLEINGOT_ARMOR_MATERIAL, 3, "tutorialBoots");
		RegisterHelper.registerItem(tutorialBoots);
	}
	
}
