package com.abe.tutorial.items;

import com.abe.tutorial.biome.features.BlockNames;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemLeafBlocks extends ItemBlock{

	public ItemLeafBlocks(Block block) {
		super(block);
		this.setHasSubtypes(true);
	}

	@Override
	public String getUnlocalizedName(ItemStack itemStack) {
		int i = itemStack.getItemDamage();
		if (i< 0 || i >= BlockNames.leaves.length){
			i = 0;
		}
		return super.getUnlocalizedName() + "." + BlockNames.leaves[i];
	}
	
	@Override
	public int getMetadata(int meta) {
		return meta;
	}

}
