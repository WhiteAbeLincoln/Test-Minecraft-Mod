package com.abe.tutorial.items;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.MovingObjectPosition;

import com.abe.tutorial.KeyBindings;
import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.util.CreationTools;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemUnstableSword extends ItemSword{

	public ItemUnstableSword(ToolMaterial swordToolMaterial) {
		super(swordToolMaterial);
		setUnlocalizedName("UnstableSword");
		setCreativeTab(RegisterCreativeTabs.tutorialTabCombat);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister){
		this.itemIcon = iconRegister.registerIcon(Reference.MODID + ":" + this.getUnlocalizedName().substring(5));
	}
	
	//overrides the super onLeftClickEntity
	public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity)
    {
		//makes sure it is run only on client
		if ((!player.worldObj.isRemote)){
			
			//creates an explosion (player to emanate from, pX, pY, pZ, strength, )
			CreationTools.createRandomExplosion(10, 2.5F, player.worldObj, (int)entity.posX, (int)entity.posY, (int)entity.posZ);
			//entity.worldObj.createExplosion(player, entity.lastTickPosX, entity.lastTickPosY, entity.lastTickPosZ, 2.5F, true);
			//entity.worldObj.playSoundAtEntity(player, par2Str, par3, par4);
		}
        return false;
    }
}