package com.abe.tutorial.items;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.blocks.RegisterBlocks;
import com.abe.tutorial.help.Reference;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemWindmill extends Item {
	public ItemWindmill() {
		setCreativeTab(RegisterCreativeTabs.tutorialTabMachines);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister) {
		this.itemIcon = iconRegister.registerIcon(Reference.MODID + ":" + this.getUnlocalizedName().substring(5));
	}
	
	@Override
	//@SideOnly(Side.CLIENT)
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer player) {
		if (!world.isRemote){
			//System.out.println("windmill placed");
		}
		return itemStack;
	}
	
	@Override
	public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int side, float x2, float y2, float z2) {
		if (!world.isRemote){
			if (side==1 && world.getBlock(x, y, z).equals(RegisterBlocks.blockWindmillBase) && world.getBlockMetadata(x, y, z) == 5)
				world.setBlock(x, y+1, z, RegisterBlocks.blockWindmill);
			
				return true;
			}

		return false;
	}
}
