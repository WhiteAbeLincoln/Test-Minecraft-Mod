package com.abe.tutorial.items;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

import com.abe.tutorial.biome.features.BlockNames;

public class ItemSaplingBlocks extends ItemBlock{

	public ItemSaplingBlocks(Block block) {
		super(block);
		this.setHasSubtypes(true);
	}

	@Override
	public String getUnlocalizedName(ItemStack itemStack) {
		int i = itemStack.getItemDamage();
		if (i< 0 || i >= BlockNames.saplings.length){
			i = 0;
		}
		return super.getUnlocalizedName() + "." + BlockNames.saplings[i];
	}
	
	@Override
	public int getMetadata(int meta) {
		return meta;
	}
}