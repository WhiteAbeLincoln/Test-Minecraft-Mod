package com.abe.tutorial.items;

import com.abe.tutorial.KeyBindings;
import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.util.CreationTools;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.util.ChatComponentText;

public class ItemUnstableAxeEnhanced extends ItemAxe{
	
	public ItemUnstableAxeEnhanced(ToolMaterial material) {
		super(material);
		
		setUnlocalizedName("UnstableAxeEnhanced");
		setCreativeTab(RegisterCreativeTabs.tutorialTabTools);
	}

	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister){
		this.itemIcon = iconRegister.registerIcon(Reference.MODID + ":" + this.getUnlocalizedName().substring(5));
	}
	
	@Override
	public boolean onBlockStartBreak(ItemStack itemstack, int X, int Y, int Z,
			EntityPlayer player) {
		
		if ((KeyBindings.key_explode.getIsKeyPressed()) && !player.worldObj.isRemote){
			player.addChatMessage(new ChatComponentText("Explosion Imminent..."));
			
			//creates an explosion (player to emanate from, pX, pY, pZ, strength, )
			player.worldObj.createExplosion(player, X, (Y+1), Z, 2.0F, true);
		}
		
		return super.onBlockStartBreak(itemstack, X, Y, Z, player);
	}

}
