package com.abe.tutorial.items;

import java.util.ArrayList;
import java.util.List;

import com.abe.tutorial.KeyBindings;
import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.util.CreationTools;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class ItemUnstablePickaxeEnhanced extends ItemPickaxe{

	protected ItemUnstablePickaxeEnhanced(ToolMaterial material) {
		super(material);
		
		setUnlocalizedName("UnstablePickaxeEnhanced");
		setCreativeTab(RegisterCreativeTabs.tutorialTabTools);
	}

	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister){
		this.itemIcon = iconRegister.registerIcon(Reference.MODID + ":" + this.getUnlocalizedName().substring(5));
	}
	
	@Override
	public boolean onBlockStartBreak(ItemStack itemstack, int X, int Y, int Z,
			EntityPlayer player) {
		
		if ((KeyBindings.key_explode.getIsKeyPressed()) && !player.worldObj.isRemote){
			World world = player.worldObj;
			List<int[]> location = new ArrayList<int[]>();
			int direction = MathHelper.floor_double((double)((player.rotationYaw * 4F) / 360F) + 0.5D) & 3;
			int depth = 3;
			/*      1            2            3
			 * ---------------------------------------
			 *            |            |            |
			 *            |            |            |
			A *           |            |            |
			 *            |            |            |
			 *            |            |            |
			 *---------------------------------------
			 *            |            |            | 
			 *            |            |            |
			B *           |            |            | 
			 *            |            |            |
			 *            |            |            |
			 *---------------------------------------
			 *            |            |            |
			C *           |            |            |
			 *            |            |            |
			 *            |            |            |
			 *---------------------------------------
			*/
			//if player is facing south
			switch (direction) {
			case 0:	//south
				for (int i = -1; i < depth - 1; i++){
					location.add(new int[]{(X+1), (Y+1), (Z+i)});//A1
					location.add(new int[]{X, (Y+1), (Z+i)});	//A2
					location.add(new int[]{(X-1), (Y+1), (Z+i)});//A3
					location.add(new int[]{(X+1), Y, (Z+i)});	//B1
					location.add(new int[]{X, Y, (Z+i)});		//B2
					location.add(new int[]{(X-1), Y, (Z+i)});	//B3
					location.add(new int[]{(X+1), (Y-1), (Z+i)});//C1
					location.add(new int[]{X, (Y-1), (Z+i)});	//C2
					location.add(new int[]{(X-1), (Y-1), (Z+i)});//C3
				}
				break;
			case 1:	//west
				for (int i = -1; i < depth -1; i++){
					location.add(new int[]{(X-i), (Y+1), (Z+1)});//A1
					location.add(new int[]{(X-i), (Y+1), Z});	//A2
					location.add(new int[]{(X-i), (Y+1), (Z-1)});//A3
					location.add(new int[]{(X-i), Y, (Z+1)});	//B1
					location.add(new int[]{(X-i), Y, Z});		//B2
					location.add(new int[]{(X-i), Y, (Z-1)});	//B3
					location.add(new int[]{(X-i), (Y-1), (Z+1)});//C1
					location.add(new int[]{(X-i), (Y-1), Z});	//C2
					location.add(new int[]{(X-i), (Y-1), (Z-1)});//C3
				}
				break;
			case 2: //north
				for (int i = -1; i < depth -1; i++){
					location.add(new int[]{(X-1), (Y+1), (Z-i)});//A1
					location.add(new int[]{X, (Y+1), (Z-i)});	//A2
					location.add(new int[]{(X+1), (Y+1), (Z-i)});//A3
					location.add(new int[]{(X-1), Y, (Z-i)});	//B1
					location.add(new int[]{X, Y, (Z-i)});		//B2
					location.add(new int[]{(X+1), Y, (Z-i)});	//B3
					location.add(new int[]{(X-1), (Y-1), (Z-i)});//C1
					location.add(new int[]{X, (Y-1), (Z-i)});	//C2
					location.add(new int[]{(X+1), (Y-1), (Z-i)});//C3
				}
				break;
			case 3:	//east
				for (int i = -1; i < depth-1; i++){
					location.add(new int[]{(X+i), (Y+1), (Z-1)});//A1
					location.add(new int[]{(X+i), (Y+1), Z});	//A2
					location.add(new int[]{(X+i), (Y+1), (Z+1)});//A3
					location.add(new int[]{(X+i), Y, (Z-1)});	//B1
					location.add(new int[]{(X+i), Y, Z});		//B2
					location.add(new int[]{(X+i), Y, (Z+1)});	//B3
					location.add(new int[]{(X+i), (Y-1), (Z-1)});//C1
					location.add(new int[]{(X+i), (Y-1), Z});	//C2
					location.add(new int[]{(X+i), (Y-1), (Z+1)});//C3
				}
				break;
			}			
			
			
			for (int i = 0; i < location.size(); i++){
				int x = location.get(i)[0];
				int y = location.get(i)[1];
				int z = location.get(i)[2];
				
				if (/*world.getBlock(x, y, z) != Blocks.bedrock &&*/ world.getBlock(x, y, z).isBlockNormalCube()){
					if (world.getBlock(x, y, z).getHarvestLevel(world.getBlockMetadata(x, y, z)) != -1 
							&& world.getBlock(x, y, z).getHarvestLevel(world.getBlockMetadata(x, y, z)) 
							<= RegisterItems.itemUnstablePickaxeEnhanced.getHarvestLevel(new ItemStack(this), "pickaxe")){
						world.playSoundEffect(x, y, z, "random.explode", 4.0F, (1.0F + (world.rand.nextFloat() - world.rand.nextFloat()) * 0.2F) * 0.7F);
						world.getBlock(x, y, z).dropBlockAsItem(world, x, y, z, world.getBlockMetadata(x, y, z), 0);
						world.setBlockToAir(x, y, z);
						//ItemStack inUseItem = player.getItemInUse();
						//player.getItemInUse().setItemDamage(inUseItem.getItemDamage() - 1);
					}
				}
			}
			//creates an explosion (player to emanate from, pX, pY, pZ, strength, )
			//player.worldObj.createExplosion(player, X, Y, Z, 4.5F, true);
			//CreationTools.createExplosion(player, X, Y, Z, 2.5F, false, true);
			location.clear();
		}
		return super.onBlockStartBreak(itemstack, X, Y, Z, player);
	}
		

}
