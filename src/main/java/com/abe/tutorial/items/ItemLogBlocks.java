package com.abe.tutorial.items;

import com.abe.tutorial.biome.features.BlockNames;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemLogBlocks extends ItemBlock{

	public ItemLogBlocks(Block block) {
		super(block);
		this.setHasSubtypes(true);
	}

	@Override
	public String getUnlocalizedName(ItemStack itemStack) {
		int i = itemStack.getItemDamage();
		if (i< 0 || i >= BlockNames.logs.length){
			i = 0;
		}
		return super.getUnlocalizedName() + "." + BlockNames.logs[i];
	}
	
	@Override
	public int getMetadata(int meta) {
		return meta;
	}
}
