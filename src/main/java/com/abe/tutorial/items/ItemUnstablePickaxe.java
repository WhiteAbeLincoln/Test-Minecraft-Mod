package com.abe.tutorial.items;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.util.CreationTools;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;


//TODO add damage when using special
public class ItemUnstablePickaxe extends ItemPickaxe {

	protected ItemUnstablePickaxe(ToolMaterial material) {
		super(material);
		
		setUnlocalizedName("UnstablePickaxe");
		setCreativeTab(RegisterCreativeTabs.tutorialTabTools);
	}

	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister){
		this.itemIcon = iconRegister.registerIcon(Reference.MODID + ":" + this.getUnlocalizedName().substring(5));
	}
	
	@Override
	public boolean onBlockStartBreak(ItemStack itemstack, int X, int Y, int Z,
			EntityPlayer player) {
		
		MovingObjectPosition position = player.rayTrace(100, 2.0F);
		
		//y=height z=forward/back x=left/right 
		
		//sidehit bottom = 0 top = 1 east = 2 west = 3 north = 4 south = 5
		//North=Z- South=Z+ East=X+ West=X-
		int x, y, z;
		x = X;
		y = Y;
		z = Z;
		if (position.sideHit == 0) y = (Y + 2);
		if (position.sideHit == 1) y = (Y - 2);
		
		if (position.sideHit == 2) x = (X - 2);
		if (position.sideHit == 3) x = (X + 2);
		
		if (position.sideHit == 4) z = (Z + 2);
		if (position.sideHit == 5) z = (Z - 2);	
		
		CreationTools.createRandomExplosion(10, 3.5F, player.worldObj, x, y, z);
		
		return super.onBlockStartBreak(itemstack, X, Y, Z, player);
	}

}
