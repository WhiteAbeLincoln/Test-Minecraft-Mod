package com.abe.tutorial.items;

import com.abe.tutorial.KeyBindings;
import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.help.Reference;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;

import org.lwjgl.input.Keyboard;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class ItemUnstableSwordEnhanced extends ItemSword{

	public ItemUnstableSwordEnhanced(ToolMaterial swordToolMaterial) {
		super(swordToolMaterial);
		setUnlocalizedName("UnstableSwordEnhanced");
		setCreativeTab(RegisterCreativeTabs.tutorialTabCombat);
	}
	
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister){
		this.itemIcon = iconRegister.registerIcon(Reference.MODID + ":" + this.getUnlocalizedName().substring(5));
	}
	
	//overrides the super onLeftClickEntity
	public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity)
    {
		//makes sure it is run only on client
		if ((!player.worldObj.isRemote) && (KeyBindings.key_explode.getIsKeyPressed())){
			
			MovingObjectPosition position = player.rayTrace(100, 2.0F);
			
			
			//spawns lightning bolt (current world, pX, pY, pZ)
			//player.worldObj.spawnEntityInWorld(new EntityLightningBolt(player.worldObj, position.blockX, position.blockY, position.blockZ));
			
			System.out.println("Creating Explosion");
			player.addChatMessage(new ChatComponentText("Explosion Imminent..."));
			
			//creates an explosion (player to emanate from, pX, pY, pZ, strength, )
			entity.worldObj.createExplosion(player, entity.lastTickPosX, entity.lastTickPosY, entity.lastTickPosZ, 2.5F, true);
			//entity.worldObj.playSoundAtEntity(player, par2Str, par3, par4);
			System.out.println("Explosion Created");
		}
        return false;
    }
}
