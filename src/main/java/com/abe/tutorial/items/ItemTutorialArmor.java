package com.abe.tutorial.items;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.help.Reference;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class ItemTutorialArmor extends ItemArmor {

	public ItemTutorialArmor(ArmorMaterial material, int armorType, String name) {
		
		//middle param is for the render type, which is only used by vanilla armors
		super(material, 0, armorType);
		setUnlocalizedName(name);
		setTextureName(Reference.MODID + ":" + getUnlocalizedName().substring(5));
		setCreativeTab(RegisterCreativeTabs.tutorialTabCombat);
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type){
		if (stack.getItem() == RegisterItems.tutorialHelmet){
			return Reference.MODID + ":models/armor/tutorialArmor1.png";
		} else if (stack.getItem() == RegisterItems.tutorialBoots){
			return Reference.MODID + ":models/armor/tutorialArmor1.png";
		} else if (stack.getItem() == RegisterItems.tutorialChest){
			return Reference.MODID + ":models/armor/tutorialArmor1.png";
		} else if (stack.getItem() == RegisterItems.tutorialPants){
			return Reference.MODID + ":models/armor/tutorialArmor2.png";
		}
		else{
			System.out.println("Error--TutorialMod: Invalid Item " + getUnlocalizedName().substring(5));
			return null;
		}
	}
	
}
