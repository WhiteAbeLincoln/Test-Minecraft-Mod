package com.abe.tutorial.container;

import com.abe.tutorial.tileEntity.TileEntityAlabasterOven;
import com.abe.tutorial.tileEntity.TileEntityStructureController;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;

public class ContainerStructureController extends Container{

private TileEntityStructureController alabasterOven;
	
	public int lastBurnTime;
	public int lastCurrentItemBurnTime;
	public int lastCookTime;	
	
	public ContainerStructureController(InventoryPlayer inventory, TileEntityStructureController tileEntity){
		this.alabasterOven = tileEntity;
		
		//adds slots to the container params: (theTileEntity, theSlotNumber(defined in the tileEntity class(slots[]), theXCoord (on theGUI), theYCoord)
		this.addSlotToContainer(new Slot(tileEntity, 0, 8, 62));
		
		//adds the players Inventory slots
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 9; j++) {
				this.addSlotToContainer(new Slot(inventory, j+(i*9)+9, 8+(j*18), 84 + i * 18));
			}
		}
		
		//adds the players Hotbar slots
		//one inventory box is 18px across, and is offset 8px from the left
		for(int i = 0; i < 9; i++) {
			this.addSlotToContainer(new Slot(inventory, i, 8 + i * 18, 142));
		}
	}
		
	//detects changes and updates the progress bars
	public void detectAndSendChanges() {
		super.detectAndSendChanges();
		
	}

	public ItemStack transferStackInSlot(EntityPlayer player, int slotNum)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(slotNum);

        if (slot != null && slot.getHasStack()) {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (slotNum == 0) {
                if (!this.mergeItemStack(itemstack1, 4, 37, true)) {
                    return null;
                }

                slot.onSlotChange(itemstack1, itemstack);
            }
            else if (slotNum != 0) {
                if (!this.mergeItemStack(itemstack1, 0, 1, false))
                {
                    return null;
                }
            }

            if (itemstack1.stackSize == 0) {
                slot.putStack((ItemStack)null);
            } else {
                slot.onSlotChanged();
            }

            if (itemstack1.stackSize == itemstack.stackSize) {
                return null;
            }

            slot.onPickupFromSlot(player, itemstack1);
        }

        return itemstack;
    }
	
	public boolean canInteractWith(EntityPlayer var1) {
		return true;
	}

}
