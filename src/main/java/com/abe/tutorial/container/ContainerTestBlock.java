package com.abe.tutorial.container;

import com.abe.tutorial.crafting.WorkSurfaceCraftingManager;
import com.abe.tutorial.tileEntity.TileEntityTestBlock;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ContainerTestBlock extends Container {

	/** The crafting matrix inventory (5x5). */
	public InventoryCrafting craftMatrix;
	public IInventory craftResult;
	private TileEntityTestBlock logic;
	private World worldObj;
	private int posX;
	private int posY;
	private int posZ;

	public ContainerTestBlock(InventoryPlayer inventoryPlayer, TileEntityTestBlock logic, int x, int y, int z) {
		this.worldObj = logic.getWorldObj();
		this.posX = x;
		this.posY = y;
		this.posZ = z;
		this.logic = logic;
		craftMatrix = new InventoryTestBlock(this, 5, 5, logic);
		craftResult = new InventoryTestBlockResult(logic);

		this.addSlotToContainer(new SlotCrafting(inventoryPlayer.player,
				craftMatrix, craftResult, 0, 141, 43));

		//adds battery slot 
		this.addSlotToContainer(new Slot(logic, 26, 111, 63));
		
		int row;
		int column;

		for (row = 0; row < 5; ++row) {
			for (column = 0; column < 5; ++column) {
				this.addSlotToContainer(new Slot(this.craftMatrix, column+row*5, 8+column*18, 7+row*18));
			}
		}
		
		// Player Inventory
        for (row = 0; row < 3; ++row)
        {
            for (column = 0; column < 9; ++column)
            {
                this.addSlotToContainer(new Slot(inventoryPlayer, column+row*9 + 9, 8 + column * 18, 106 + row * 18));
            }
        }
        
        for (column = 0; column < 9; ++column)
        {
            this.addSlotToContainer(new Slot(inventoryPlayer, column, 8 + column * 18, 164));
        }
        
        onCraftMatrixChanged(craftMatrix);
	}
	
	@Override
	public void onCraftMatrixChanged(IInventory iInventory) {
		//System.out.println(logic.storage.getEnergyStored());
		if (logic.storage.getEnergyStored() > 0){
			craftResult.setInventorySlotContents(0, WorkSurfaceCraftingManager.getInstance().findMatchingRecipe(craftMatrix, worldObj));
			logic.storage.extractEnergy(50, false);
			//System.out.println(logic.storage.getEnergyStored());
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer player) {
		return player.getDistanceSq((double) this.posX + 0.5D, (double) this.posY + 0.5D, (double) this.posZ + 0.5D) <= 64.0D;
	}
	
	public ItemStack transferStackInSlot(EntityPlayer player, int slotNum) {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(slotNum);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (slotNum == 0)
            {
                if (!this.mergeItemStack(itemstack1, 10, 46, true))
                {
                    return null;
                }

                slot.onSlotChange(itemstack1, itemstack);
            }
            else if (slotNum >= 10 && slotNum < 37)
            {
                if (!this.mergeItemStack(itemstack1, 37, 46, false))
                {
                    return null;
                }
            }
            else if (slotNum >= 37 && slotNum < 46)
            {
                if (!this.mergeItemStack(itemstack1, 10, 37, false))
                {
                    return null;
                }
            }
            else if (!this.mergeItemStack(itemstack1, 10, 46, false))
            {
                return null;
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }

            if (itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }

            slot.onPickupFromSlot(player, itemstack1);
        }

        return itemstack;
    }
	
	@Override
    public boolean func_94530_a (ItemStack par1ItemStack, Slot par2Slot)
    {
        return par2Slot.inventory != this.craftResult && super.func_94530_a(par1ItemStack, par2Slot);
    }

}
