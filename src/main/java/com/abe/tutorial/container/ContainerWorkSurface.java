package com.abe.tutorial.container;

import com.abe.tutorial.blocks.RegisterBlocks;
import com.abe.tutorial.blocks.WorkSurface;
import com.abe.tutorial.crafting.WorkSurfaceCraftingManager;
import com.abe.tutorial.tileEntity.TileEntityAlabasterOven;
import com.abe.tutorial.tileEntity.TileEntityWorkSurface;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryCraftResult;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.world.World;

public class ContainerWorkSurface extends Container {

	public InventoryCrafting craftMatrix;
	public IInventory craftResult;
	private World worldObj;
	private TileEntityWorkSurface workSurface;
	private int posX;
	private int posY;
	private int posZ;
	
	public ContainerWorkSurface(InventoryPlayer inventoryPlayer, TileEntityWorkSurface teWorkSurface){
		craftMatrix = new InventoryCrafting(this, 5, 5);		//sets the size of the matrix
		craftResult = new InventoryCraftResult();
		workSurface = teWorkSurface;
		worldObj = teWorkSurface.getWorldObj();
		posX = teWorkSurface.xCoord;
		posY = teWorkSurface.yCoord;
		posZ = teWorkSurface.zCoord;
		
		this.addSlotToContainer(new SlotCrafting(inventoryPlayer.player, craftMatrix, craftResult, 0, 141, 43));
		
		//adds battery slot
		this.addSlotToContainer(new Slot(teWorkSurface, 61, 111, 63));
		
		//adds crafting slots to container
		for (int i = 0; i < 5; i++){
			for (int k = 0; k < 5; k++){
				//slot parameters(inventory, slotIndex, xPos, yPos)
				this.addSlotToContainer(new Slot(craftMatrix, k+i*5, 8+k*18, 7+i*18));
				//System.out.println(k+i*5);
			}
		}
		
		//adds the players Hotbar slots
		//one inventory box is 18px across, and is offset 8px from the left
		for(int i = 0; i < 9; i++) {
			this.addSlotToContainer(new Slot(inventoryPlayer, i, 8 + i * 18, 164));
		}
		
		//adds player slots
		for (int i = 0; i< 3; i++){
			for (int k=0; k < 9; k++){
				this.addSlotToContainer(new Slot(inventoryPlayer, k + i*9+9, 8+k*18, 106+i*18));
			}
		}
		
		onCraftMatrixChanged(craftMatrix);
	}
	
	public ItemStack transferStackInSlot(EntityPlayer player, int slotNum) {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(slotNum);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (slotNum == 0)
            {
                if (!this.mergeItemStack(itemstack1, 10, 46, true))
                {
                    return null;
                }

                slot.onSlotChange(itemstack1, itemstack);
            }
            else if (slotNum >= 10 && slotNum < 37)
            {
                if (!this.mergeItemStack(itemstack1, 37, 46, false))
                {
                    return null;
                }
            }
            else if (slotNum >= 37 && slotNum < 46)
            {
                if (!this.mergeItemStack(itemstack1, 10, 37, false))
                {
                    return null;
                }
            }
            else if (!this.mergeItemStack(itemstack1, 10, 46, false))
            {
                return null;
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }

            if (itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }

            slot.onPickupFromSlot(player, itemstack1);
        }

        return itemstack;
    }
	
	@Override
	public void onCraftMatrixChanged(IInventory iInventory) {
		craftResult.setInventorySlotContents(0, WorkSurfaceCraftingManager.getInstance().findMatchingRecipe(craftMatrix, worldObj));
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer player) {		
		return workSurface.isUseableByPlayer(player);
	}
	
	/*public void onContainerClosed(EntityPlayer par1EntityPlayer) {
        super.onContainerClosed(par1EntityPlayer);

        if (!this.worldObj.isRemote)
        {
            for (int i = 0; i < 25; ++i)
            {
                ItemStack itemstack = this.craftMatrix.getStackInSlotOnClosing(i);

                if (itemstack != null)
                {
                    par1EntityPlayer.dropPlayerItemWithRandomChoice(itemstack, false);
                }
            }
            
        }
    }*/

}
