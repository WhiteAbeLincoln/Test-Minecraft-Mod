package com.abe.tutorial.container;

import com.abe.tutorial.container.slot.SlotIngotMasher;
import com.abe.tutorial.tileEntity.TileEntityIngotMasher;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerIngotMasher extends Container {

	private TileEntityIngotMasher ingotMasher;
	private int dualCookTime;
	private int dualPower;
	private int lastItemBurnTime;

	public ContainerIngotMasher(InventoryPlayer invPlayer, TileEntityIngotMasher teIngotMasher) {
		dualCookTime = 0;
		dualPower = 0;
		lastItemBurnTime = 0;
		ingotMasher = teIngotMasher;

		// inventory, slotIndex, x, y
		this.addSlotToContainer(new Slot(teIngotMasher, 0, 46, 17));//top input
		this.addSlotToContainer(new Slot(teIngotMasher, 1, 46, 49));//bottom input
		this.addSlotToContainer(new Slot(teIngotMasher, 2, 8, 56));//fuel input
		this.addSlotToContainer(new SlotIngotMasher(invPlayer.player, teIngotMasher, 3, 113, 33));//output

		// ActionBar
		for (int i = 0; i < 9; i++) {
			this.addSlotToContainer(new Slot(invPlayer, i, 8 + i * 18, 142));
			//adds slots with index in range of 4 to 13
		}
		
		// Inventory
		for (int i = 0; i < 3; i++) { //rows
			for (int j = 0; j < 9; j++) {//columns
				this.addSlotToContainer(new Slot(invPlayer, j + i*9 + 9, 8 + j * 18, 84 + i * 18));
					//adds slots with index in the range of 13 to 39
			}
		}
	}

	@Override
	public void addCraftingToCrafters(ICrafting crafting) {
		super.addCraftingToCrafters(crafting);
		crafting.sendProgressBarUpdate(this, 0, this.ingotMasher.dualCookTime);
		crafting.sendProgressBarUpdate(this, 0, this.ingotMasher.dualPower);
	}

	public ItemStack transferStackInSlot(EntityPlayer player, int slotNum) {
		ItemStack itemstack = null;
		Slot slot = (Slot)this.inventorySlots.get(slotNum);

		if (slot != null && slot.getHasStack()) {		//if there is an itemstack in the slot
			ItemStack itemstack1 = slot.getStack();		//stores the itemstack into a variable
			itemstack = itemstack1.copy();

			
			if (slotNum >= 0 && slotNum <= 3) {			//if the slot is one of the input/output slots(numbers 0 to 3)
														//mergeItemStack params: (itemstack, startSlot, endSlot+1 , startAtEnd?)
				if (!this.mergeItemStack(itemstack1, 4, 40, true)) {		//tries to merge the stack with one in slots 4-39(player inventory and hotbar) **remember that the endSlot param requires you add 1 to the real value (slot 39 would change to 40)
					return null;
				}
				slot.onSlotChange(itemstack1, itemstack);
				
			} else if (slotNum >= 4 && slotNum < 39) {						//if the slot is one of the player slots(numbers 4-39)
				//TODO check if has smelting recipe
				if (ingotMasher.hasItemPower(itemstack1)) {					//if the itemstack is fuel
					if (!this.mergeItemStack(itemstack1, 2, 3, false)) {	//try to store in fuel slot (slot 2)
						return null;
					}
				} else {
					if (!this.mergeItemStack(itemstack1, 0, 2, false)) {	//try to store in inputs **notice no method to try to store in output (you dont want to be putting things there)
						return null;
					}
				}
			} else if (!this.mergeItemStack(itemstack1, 4, 40, false)) {	//failsafe
				return null;
			}

			if (itemstack1.stackSize == 0) {		//if there is nothing in the stack
				slot.putStack((ItemStack) null);	//put an empty stack in the slot you clicked
			} else {
				slot.onSlotChanged();
			}

			if (itemstack1.stackSize == itemstack.stackSize) {
				return null;
			}

			slot.onPickupFromSlot(player, itemstack1);
		}

		return itemstack;
	}

	@Override
	public boolean canInteractWith(EntityPlayer player) {
		// TODO Auto-generated method stub
		return ingotMasher.isUseableByPlayer(player);
	}
	
	@Override
	public void detectAndSendChanges() {
		super.detectAndSendChanges();
		
		for (int i= 0; i<this.crafters.size(); i++){
			ICrafting par1 = (ICrafting)this.crafters.get(i);
			
			if (this.dualCookTime != this.ingotMasher.dualCookTime){
				par1.sendProgressBarUpdate(this, 0, this.ingotMasher.dualCookTime);
			}
			
			if (this.dualPower != this.ingotMasher.dualPower){
				par1.sendProgressBarUpdate(this, 1, this.ingotMasher.dualPower);
			}
		}
		
		this.dualCookTime = this.ingotMasher.dualCookTime;
		this.dualPower = this.ingotMasher.dualPower;
	}
	
	@Override
	public void updateProgressBar(int i, int j) {
		if (i==0){
			ingotMasher.dualCookTime = j;
		}
		if (i==1){
			ingotMasher.dualPower = j;
		}
		super.updateProgressBar(i, j);
	}

}
