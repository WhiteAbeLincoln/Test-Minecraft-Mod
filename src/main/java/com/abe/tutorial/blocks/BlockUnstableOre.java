package com.abe.tutorial.blocks;

import java.util.Random;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.util.CreationTools;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.util.DamageSource;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

public class BlockUnstableOre extends Block{
	
	public BlockUnstableOre(){
		super(Material.rock);						//sets the material that this is, which is contained in the super class (Block)
		setBlockName("tutorialOreBlock");			//sets the blocks name
		setBlockTextureName(Reference.MODID + ":" + getUnlocalizedName().substring(5));			//sets the name of the texture (used to find the texture file)
		setCreativeTab(RegisterCreativeTabs.tutorialTabBlocks);		//sets what tab it is in in the creative menu
		setStepSound(soundTypeStone);				//sets the sound it makes when stepped on
		setHardness(3.0F);							//sets the hardness of it (how long to break)
		setResistance(5.0F);						//sets the resistance to explosions
		setLightLevel(10.0F);						//sets the light level it gives off (if less than the light level around it gives of darkness)
		setHarvestLevel("pickaxe", 2);				//sets the required tools to harvest (mine in this case) a block
		setTickRandomly(true);
	}
	
	//for blocks that have transparency, renders the blocks around them
	//do this by overriding the isOpaqueCube method of Block
	/*@Override
	public boolean isOpaqueCube()
	{
		return false;
	}*/
	
	@Override
	public void onEntityWalking(World world, int posX,
			int posY, int posZ, Entity entity) {
		
		if (!world.isRemote){
			CreationTools.createRandomExplosion(10, 6.5F, world, entity);
		}
		super.onEntityWalking(world, posX, posY, posZ,
				entity);
	}
	
	@Override
	public void onBlockDestroyedByPlayer(World world, int x, int y, int z, int metadata)
    {
        if (!world.isRemote){
        	CreationTools.createRandomExplosion(8, 3.5F, world, x, y, z);
        }
    }
	
	// explodes on random ticks if the player is within 10 blocks
		@Override
		@SideOnly(Side.CLIENT)
		public void updateTick(World world, int x, int y,
				int z, Random random) {
			
			if ((world.getClosestPlayer(x, y, z, 10) != null) && (!world.isRemote)){
				CreationTools.createRandomExplosion(30, 3.5F, world, x, y, z);
				System.out.println("Block Updated");
			}
			
			super.updateTick(world, x, y, z,
					random);
		}
	
	
	
}
