package com.abe.tutorial.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.biome.features.TutLeaf;
import com.abe.tutorial.biome.features.TutLog;
import com.abe.tutorial.biome.features.TutSapling;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.help.RegisterHelper;
import com.abe.tutorial.items.ItemLeafBlocks;
import com.abe.tutorial.items.ItemLogBlocks;
import com.abe.tutorial.items.ItemMetadataBlocks;
import com.abe.tutorial.items.ItemSaplingBlocks;
import com.abe.tutorial.items.RegisterItems;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class RegisterBlocks {

	public static Block oreUnstableOre;
	public static Block tutorialMetaDataBlock;
	
	public static Block oreTinOreBlock;
	public static Block oreZincOreBlock;
	public static Block oreNickelOreBlock;
	public static Block oreManganeseOreBlock;
	public static Block oreVanadiumOreBlock;
	public static Block oreRhodiumOreBlock;
	public static Block oreAlabasterBlock;
	public static Block oreTopazOre;
	
	public static Block oreNetherZincOreBlock;
	
	public static Block blockAlabasterOvenIdle;
	public static Block blockAlabasterOvenActive;
	public static final int guiIDAlabasterOven = 0;
	
	public static Block blockWorkSurface;
	public static final int guiIDWorkSurface = 1;
	
	public static Block blockTestBlock;
	public static final int guiIDTestBlock = 3;
	
	public static Block blockIngotMasherIdle;
	public static Block blockIngotMasherActive;
	public static final int guiIDIngotMasher = 2;
	
	public static Block blockWindmill;
	public static Block blockWindmillBase;
	
	public static Block blockUnstableBlock;
	public static Block blockObsidianTable;
	
	public static Block blockControlBlock;
	
	//trees
	public static Block blockLog;
	public static Block blockLeaf;
	public static Block blockSapling;
	
	public static Block blockStructureController;
	public static final int guiIDStructureController = 4;
	
	public static void loadBlocks(){
		//machines
		blockAlabasterOvenIdle = new AlabasterOven(false).setBlockName("AlabasterOvenIdle").setCreativeTab(RegisterCreativeTabs.tutorialTabMachines);
		RegisterHelper.registerBlock(blockAlabasterOvenIdle);
		blockAlabasterOvenActive = new AlabasterOven(true).setBlockName("AlabasterOvenActive").setLightLevel(0.625F);
		RegisterHelper.registerBlock(blockAlabasterOvenActive);
		
		blockWorkSurface = new WorkSurface().setBlockName("WorkSurface");
		RegisterHelper.registerBlock(blockWorkSurface);
		
		blockTestBlock = new TestBlock(Material.iron).setBlockName("TestBlock");
		RegisterHelper.registerBlock(blockTestBlock);
		
		blockIngotMasherIdle = new IngotMasher(false).setBlockName("IngotMasherIdle").setCreativeTab(RegisterCreativeTabs.tutorialTabMachines);
		RegisterHelper.registerBlock(blockIngotMasherIdle);
		blockIngotMasherActive = new IngotMasher(true).setBlockName("IngotMasherActive");
		RegisterHelper.registerBlock(blockIngotMasherActive);
		
		blockWindmill = new BlockWindmill().setBlockName("blockWindmill");
		RegisterHelper.registerBlock(blockWindmill);
		
		blockWindmillBase = new BlockWindmillBase().setBlockName("WindmillBase");
		RegisterHelper.registerBlock(blockWindmillBase);
		
		//meta
		tutorialMetaDataBlock = new BlockMetadataBlock();
		RegisterHelper.registerBlock(tutorialMetaDataBlock, ItemMetadataBlocks.class);
		
		//ores
		oreUnstableOre = new BlockUnstableOre();
		RegisterHelper.registerBlock(oreUnstableOre);
		
		oreTinOreBlock = new OreBlock(Material.rock, null, 1).setBlockName("TinOre");
		RegisterHelper.registerBlock(oreTinOreBlock);
		
		oreZincOreBlock = new OreBlock(Material.rock, null, 1).setBlockName("ZincOre");
		RegisterHelper.registerBlock(oreZincOreBlock);
		
		oreNickelOreBlock = new OreBlock(Material.rock, null, 1).setBlockName("NickelOre");
		RegisterHelper.registerBlock(oreNickelOreBlock);

		oreManganeseOreBlock = new OreBlock(Material.rock, null, 1).setBlockName("ManganeseOre");
		RegisterHelper.registerBlock(oreManganeseOreBlock);

		oreVanadiumOreBlock = new OreBlock(Material.rock, null, 1).setBlockName("VanadiumOre");
		RegisterHelper.registerBlock(oreVanadiumOreBlock);
		
		oreRhodiumOreBlock = new OreBlock(Material.rock, null, 1).setBlockName("RhodiumOre");
		RegisterHelper.registerBlock(oreRhodiumOreBlock);
		
		oreTopazOre = new OreBlock(Material.rock, RegisterItems.itemTopaz, 3).setBlockName("TopazOre");
		RegisterHelper.registerBlock(oreTopazOre);
		
		oreAlabasterBlock = new OreBlock(Material.rock, null, 1).setBlockName("Alabaster");
		RegisterHelper.registerBlock(oreAlabasterBlock);
		
		oreNetherZincOreBlock = new OreBlock(Material.rock, null, 1).setBlockName("NetherZincOre");
		RegisterHelper.registerBlock(oreNetherZincOreBlock);
		
		//blocks
		blockUnstableBlock = new IngotBlock(Material.iron).setBlockName("UnstableBlock");
		RegisterHelper.registerBlock(blockUnstableBlock);
		
		blockObsidianTable = new ObsidianTable(Material.rock).setBlockName("ObsidianTable");
		RegisterHelper.registerBlock(blockObsidianTable);
		
		blockControlBlock = new ControlBlock().setBlockName("ControlBlock");
		RegisterHelper.registerBlock(blockControlBlock);
		
		//trees
		blockLog = new TutLog().setBlockName("Log").setCreativeTab(RegisterCreativeTabs.tutorialTabBlocks);
		RegisterHelper.registerBlock(blockLog, ItemLogBlocks.class);
		
		blockLeaf = new TutLeaf().setBlockName("Leaf").setCreativeTab(RegisterCreativeTabs.tutorialTabBlocks);
		RegisterHelper.registerBlock(blockLeaf, ItemLeafBlocks.class);
		
		blockSapling = new TutSapling().setBlockName("Sapling").setCreativeTab(RegisterCreativeTabs.tutorialTabItems);
		RegisterHelper.registerBlock(blockSapling, ItemSaplingBlocks.class);
		
		
		blockStructureController = new StructureController().setBlockName("StructureController").setCreativeTab(RegisterCreativeTabs.tutorialTabMachines);
		RegisterHelper.registerBlock(blockStructureController);
	}
}
