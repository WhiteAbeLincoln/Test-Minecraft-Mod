package com.abe.tutorial.blocks;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.TutorialMod;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.tileEntity.TileEntityTestBlock;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import mantle.blocks.abstracts.InventoryBlock;

public class TestBlock extends InventoryBlock{

	protected TestBlock(Material material) {
		super(material);
		this.setHardness(3.5F);
		this.setStepSound(soundTypeMetal);
		this.setCreativeTab(RegisterCreativeTabs.tutorialTabMachines);
	}

	@Override
	public String[] getTextureNames() {
		String[] textureNames = { "WorkSurfaceSide", "WorkSurfaceTop", "WorkSurfaceBottom"};
		return textureNames;
	}
	
	@Override
	public String getTextureDomain(int arg0) {
		return Reference.MODID;
	}
	
	@Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon (int side, int meta)
    {
        return icons[getTextureIndex(side)];
    }
	
	public int getTextureIndex (int side)
    {
        if (side == 0) //bottom
            return 2;
        if (side == 1)	//top
            return 1;

        return 0;
    }
	
	@Override
	public Integer getGui(World world, int x, int y, int z, EntityPlayer player) {
		return RegisterBlocks.guiIDTestBlock;
	}

	@Override
	public Object getModInstance() {
		return TutorialMod.instance;
	}
	
	@Override
	public TileEntity createNewTileEntity(World arg0, int arg1) {
		return new TileEntityTestBlock();
	}


}
