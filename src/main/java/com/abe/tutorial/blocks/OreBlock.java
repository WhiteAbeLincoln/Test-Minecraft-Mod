package com.abe.tutorial.blocks;

import java.util.Random;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.items.RegisterItems;
import com.abe.tutorial.util.CreationTools;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;

public class OreBlock extends Block {
	
	private final boolean returnItem;
	private final int dropAmt;
	private final Item item;

	
	/**
	 * Creates an OreBlock with the specified material, item to Drop, and Dropped Item
	 * @param material	the block material
	 * @param droppedItem	the item to drop when broken (enter null to drop the block)
	 * @param minDropAmt	the amount of the item to drop 
	 */
	public OreBlock(Material material, Item droppedItem, int minDropAmt){
		super(material);						//sets the material that this is, which is contained in the super class (Block)
		
		if (droppedItem != null){ returnItem = true; } else { returnItem = false;}
		item = droppedItem;
		dropAmt = minDropAmt;
		
		setHardness(3.0F);							//sets the hardness of it (how long to break)
		setResistance(3.5F);						//sets the resistance to explosions
		setStepSound(soundTypeStone);				//sets the sound it makes when stepped on
		setCreativeTab(RegisterCreativeTabs.tutorialTabBlocks);		//sets what tab it is in in the creative menu
		setHarvestLevel("pickaxe", 1);				//sets the required tools to harvest (mine in this case) a block
	}

	public Item getItemDropped(int i, Random random, int j){
		//conditional statement (if shortcut)
		//if this == oreTopazOre, then return itemTopaz, else getItemFromBlock(this)
		
		return returnItem ? item : Item.getItemFromBlock(this);
		/*if (this == RegisterBlocks.oreTopazOre){
			return RegisterItems.itemTopaz;
		} else {
			return Item.getItemFromBlock(this);
		}*/
	}
	
	public int quantityDropped(Random random){
		return returnItem ? dropAmt + random.nextInt(4) : 1;
	}
	
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister){
		this.blockIcon = iconRegister.registerIcon(Reference.MODID + ":" + getUnlocalizedName().substring(5));
	}
	
}
