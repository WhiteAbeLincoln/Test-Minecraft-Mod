package com.abe.tutorial.blocks;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.tileEntity.TileEntityWindmill;
import com.abe.tutorial.tileEntity.TileMultiBlock;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

public class BlockWindmill extends Block{

	public BlockWindmill() {
		super(Material.iron);
		setCreativeTab(RegisterCreativeTabs.tutorialTabMachines);
		setHardness(4.5F);
	}
	
	@Override
	public int getRenderType() {		//makes sure not rendered at all by minecraft standard renderer 
		return 0;
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}
	
	//@Override
	//public TileEntity createNewTileEntity(World var1, int var2) {
	//	return new TileEntityWindmill();
	//}
	
	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block neighborBlock) {
		UpdateMultiBlockStructure(world, x, y, z);
	}
	
	@Override
	public void onBlockAdded(World world, int x, int y, int z) {
		UpdateMultiBlockStructure(world, x, y, z);
	}
		
	
	public void UpdateMultiBlockStructure(World world, int x, int y, int z){
		isMultiBlockStructure(world, x, y, z);
	}
	
	public boolean isMultiBlockStructure(World world, int x, int y, int z){
		boolean isStructure = false;
		boolean currentCheckStructure = true;
		for(int y2 = 1; y2 < 6; y2++){
				if(!isStructure){
					currentCheckStructure = true;
					if (!(world.getBlock(x, y-y2, z)==RegisterBlocks.blockWindmill)){
						currentCheckStructure = false;
					}
					if (currentCheckStructure){
						world.setBlockMetadataWithNotify(x, y-y2, z, y2, 2);
					}
				}
				isStructure = currentCheckStructure;
		}
		
		if (isStructure){
			System.out.println("ISSTRUCTURE");
			return true;
		}
		if (world.getBlockMetadata(x, y, z) > 0) world.setBlockMetadataWithNotify(x, y, z, 0, 3);
		return false;
	}
	
	@Override
	public void onBlockClicked(World world, int x, int y, int z, EntityPlayer player) {
		
		player.addChatMessage(new ChatComponentText("Metadata: "+ world.getBlockMetadata(x, y, z)));
		
		super.onBlockClicked(world, x, y, z, player);
	}
	
}
