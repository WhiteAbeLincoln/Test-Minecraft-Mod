package com.abe.tutorial.blocks;

import java.util.List;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.help.Reference;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public class BlockMetadataBlock extends Block{
	@SideOnly(Side.CLIENT)
	private IIcon[] texture = new IIcon[4];
	
	public final static String[] SUBBLOCKS_STRINGS = new String[]{"limestone", "shale", "granite", "andesite"};
	
	public BlockMetadataBlock (){
		super(Material.rock);
		setHardness(3.5F);
		setResistance(5.0F);
		setBlockName("tutorialMetaDataBlock");
		setCreativeTab(RegisterCreativeTabs.tutorialTabBlocks);
	}
	
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister) {
		
		texture = new IIcon[SUBBLOCKS_STRINGS.length];
		
		for (int i = 0; i < SUBBLOCKS_STRINGS.length; i++) {
			texture[i] = iconRegister.registerIcon(Reference.MODID + ":" + getUnlocalizedName().substring(5) + "-" + SUBBLOCKS_STRINGS[i]);
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(Item block, CreativeTabs creativeTabs, List list){
		
		for(int i = 0; i < SUBBLOCKS_STRINGS.length; i++){
			list.add(new ItemStack(block, 1, i));
		}
		
	}
	
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta) {
		return texture[meta];
	}
	
	public int damageDropped(int meta) {
		return meta;
	}
	
}
