package com.abe.tutorial.blocks;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.TutorialMod;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.tileEntity.TileEntityWorkSurface;
import com.abe.tutorial.tileEntity.TileTutorial;
import com.abe.tutorial.tileEntity.base.MBController;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

public class ControlBlock extends BlockContainer{

	protected ControlBlock() {
		super(Material.iron);
		
		setHardness(3.0F);							//sets the hardness of it (how long to break)
		setResistance(5.0F);						//sets the resistance to explosions
		setStepSound(soundTypeAnvil);				//sets the sound it makes when stepped on
		setCreativeTab(RegisterCreativeTabs.tutorialTabMachines);		//sets what tab it is in in the creative menu
		setHarvestLevel("pickaxe", 1);				//sets the required tools to harvest (mine in this case) a block
	}

	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister){
		this.blockIcon = iconRegister.registerIcon(Reference.MODID + ":" + getUnlocalizedName().substring(5));
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		return new TileTutorial();
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int q, float a, float b, float c){
		if (!player.isSneaking()){
			MBController controller = (MBController) world.getTileEntity(x, y, z);
			
			if (controller.scanStructure()){
				player.addChatMessage(new ChatComponentText("asdkfjlaskd a"));
				System.out.println("aldjf");
			}
			return true;
		}else{
			return false;
		}
	}
}
