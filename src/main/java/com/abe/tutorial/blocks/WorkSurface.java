package com.abe.tutorial.blocks;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.TutorialMod;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.tileEntity.TileEntityWorkSurface;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class WorkSurface extends BlockContainer{

	@SideOnly(Side.CLIENT)
	private IIcon workSurfaceTop;
	
	//@SideOnly(Side.CLIENT)
	//private IIcon workSurfaceSide;
	
	public WorkSurface() {
		super(Material.wood);
		
		this.setHardness(3.5F);
		this.setResistance(5.0F);
		this.setCreativeTab(RegisterCreativeTabs.tutorialTabMachines);
	}
	
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int metadata){
		return side == 1 ? this.workSurfaceTop : this.blockIcon;
	}
	
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister){
		this.blockIcon = iconRegister.registerIcon(Reference.MODID + ":" + "WorkSurfaceSide");
		this.workSurfaceTop = iconRegister.registerIcon(Reference.MODID + ":" + "WorkSurfaceTop");
	}
	
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int q, float a, float b, float c){
		if (!player.isSneaking()){
			TileEntityWorkSurface tews = (TileEntityWorkSurface)world.getTileEntity(x, y, z);
			
			if (tews.scanBelow()){
				player.openGui(TutorialMod.instance, RegisterBlocks.guiIDWorkSurface, world, x, y, z);
			}
			return true;
		}else{
			return false;
		}
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		// TODO Auto-generated method stub
		return new TileEntityWorkSurface();
	}
}
