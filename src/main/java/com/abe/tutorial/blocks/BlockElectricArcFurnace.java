package com.abe.tutorial.blocks;

import com.abe.tutorial.help.Reference;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockElectricArcFurnace extends Block {

	public BlockElectricArcFurnace() {
		super(Material.rock);						//sets the material that this is, which is contained in the super class (Block)
		setBlockName("tutorialOreBlock");			//sets the blocks name
		setBlockTextureName(Reference.MODID + ":" + getUnlocalizedName().substring(5));			//sets the name of the texture (used to find the texture file)
		setCreativeTab(CreativeTabs.tabBlock);		//sets what tab it is in in the creative menu
		setStepSound(soundTypeStone);				//sets the sound it makes when stepped on
		setHardness(3.0F);							//sets the hardness of it (how long to break)
		setResistance(5.0F);						//sets the resistance to explosions
		setLightLevel(0.001F);						//sets the light level it gives off (if less than the light level around it gives of darkness)
		setHarvestLevel("pickaxe", 2);				//sets the required tools to harvest (mine in this case) a block
		setTickRandomly(true);
	}
	
}
