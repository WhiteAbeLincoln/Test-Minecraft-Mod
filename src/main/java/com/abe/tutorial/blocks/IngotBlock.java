package com.abe.tutorial.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.help.Reference;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class IngotBlock extends Block{

	public IngotBlock(Material material){
		super(material);						//sets the material that this is, which is contained in the super class (Block)
		
		setHardness(5.0F);							//sets the hardness of it (how long to break)
		setResistance(5.0F);						//sets the resistance to explosions
		setStepSound(soundTypeMetal);				//sets the sound it makes when stepped on
		setCreativeTab(RegisterCreativeTabs.tutorialTabBlocks);		//sets what tab it is in in the creative menu
		setHarvestLevel("pickaxe", 2);				//sets the required tools to harvest (mine in this case) a block
	}
	
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister){
		this.blockIcon = iconRegister.registerIcon(Reference.MODID + ":" + getUnlocalizedName().substring(5));
	}
	
}
