package com.abe.tutorial.blocks;

import scala.collection.mutable.ArrayBuilder.ofBoolean;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.tileEntity.TileEntityWindmillBase;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class BlockWindmillBase extends BlockContainer{

	protected BlockWindmillBase() {
		super(Material.iron);
		setCreativeTab(RegisterCreativeTabs.tutorialTabMachines);
		setHardness(4.5F);
		setResistance(5.0F);						//sets the resistance to explosions
		setBlockBounds(0, 0, 0, 1, 0.3F, 1);
	}
	
	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
	public int getRenderType() {
		return 0;
	}

	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister){
		this.blockIcon = iconRegister.registerIcon(Reference.MODID + ":" + getUnlocalizedName().substring(5));
	}
	
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}
	
	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block neighborBlock) {
		UpdateMultiBlockStructure(world, x, y, z);
	}
	
	@Override
	public void onBlockAdded(World world, int x, int y, int z) {
		UpdateMultiBlockStructure(world, x, y, z);
	}
		
	
	public void UpdateMultiBlockStructure(World world, int x, int y, int z){
		isMultiBlockStructure(world, x, y, z);
	}
	
	public boolean isMultiBlockStructure(World world, int x, int y, int z){
		boolean isStructure = false;
		boolean currentCheckStructure = true;
		for(int x2 = 0; x2 < 3; x2++){
			for(int z2=0; z2<3;z2++){
				if(!isStructure){
					currentCheckStructure = true;
					
					for (int x3 = 0;x3 < 3; x3++){
						for(int z3=0; z3 < 3; z3++){
							if (currentCheckStructure && !world.getBlock(x+x2-x3, y, z+z2-z3).equals(RegisterBlocks.blockWindmillBase)){
								currentCheckStructure = false;
							}
						}
					}
					
					if (currentCheckStructure){
						for (int x3 = 0;x3 < 3; x3++){
							for(int z3=0; z3 < 3; z3++){
								//world.setBlock(x+x2-x3, y, z+z2-z3, Blocks.bedrock);
								world.setBlockMetadataWithNotify(x+x2-x3, y, z+z2-z3, x3*3+z3+1, 2);
							}
						}
					}
				}
				isStructure = currentCheckStructure;
			}
		}
		
		if (isStructure) return true;
		if (world.getBlockMetadata(x, y, z) > 0) world.setBlockMetadataWithNotify(x, y, z, 0, 3);
		return false;
	}

	@Override
	public TileEntity createNewTileEntity(World world, int metadata) {
		return new TileEntityWindmillBase();
	}
	
	@Override
	public void onBlockClicked(World world, int x, int y, int z, EntityPlayer player) {
		
		player.addChatMessage(new ChatComponentText("Metadata: "+ world.getBlockMetadata(x, y, z)));
		
		super.onBlockClicked(world, x, y, z, player);
	}
	
}
