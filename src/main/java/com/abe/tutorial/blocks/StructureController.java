package com.abe.tutorial.blocks;

import com.abe.tutorial.RegisterCreativeTabs;
import com.abe.tutorial.TutorialMod;
import com.abe.tutorial.help.Reference;
import com.abe.tutorial.tileEntity.TileEntityStructureController;

import cpw.mods.fml.common.network.internal.FMLNetworkHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class StructureController extends BlockContainer{
	
	protected StructureController() {
		super(Material.iron);
		
		setHardness(5.0F);							//sets the hardness of it (how long to break)
		setResistance(5.0F);						//sets the resistance to explosions
		setStepSound(soundTypeMetal);				//sets the sound it makes when stepped on
		setCreativeTab(RegisterCreativeTabs.tutorialTabBlocks);		//sets what tab it is in in the creative menu
		setHarvestLevel("pickaxe", 2);				//sets the required tools to harvest (mine in this case) a block
	}
	
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister){
		this.blockIcon = iconRegister.registerIcon(Reference.MODID + ":" + getUnlocalizedName().substring(5));
	}

	public void onBlockAdded(World world, int x, int y, int z){
		super.onBlockAdded(world, x, y, z);
		this.setDefaultDirection(world, x, y, z);
		
	}
	
	private void setDefaultDirection(World world, int x, int y, int z) {
		if(!world.isRemote){
			Block b1 = world.getBlock(x, y, (z - 1));
			Block b2 = world.getBlock(x, y, (z + 1));
			Block b3 = world.getBlock((x - 1), y, z);
			Block b4 = world.getBlock((x + 1), y, z);
			
			IIcon i1 = b1.getIcon(2, 0);
			IIcon i2 = b2.getIcon(2, 0);
			IIcon i3 = b3.getIcon(2, 0);
			IIcon i4 = b4.getIcon(2, 0);
			
			/*if (i1 == i2){
				if (i2 == i3){
					this.blockIcon = i2;
				} else if (i2 == i4){
					this.blockIcon = i2;
				}
			} else if (i1 == i3){
				if (i3 == i4){
					this.blockIcon = i3;
				} else if (i3 == i2){
					this.blockIcon = i3;
				}
			} else if (i1 == i4){
				if (i4 == i3){
					this.blockIcon = i4;
				} else if (i4 == i2){
					this.blockIcon = i4;
				}
			}*/
		}
	}
	
	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		// TODO Auto-generated method stub
		return new TileEntityStructureController();
	}
	
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ){
		if (!world.isRemote){
			FMLNetworkHandler.openGui(player, TutorialMod.instance, RegisterBlocks.guiIDStructureController, world, x, y, z);
		}
		
		return true;
	}
	
}
