package com.abe.tutorial.crafting;

import java.util.Comparator;

import net.minecraft.item.crafting.IRecipe;

public class WorkSurfaceRecipeSorter implements Comparator {

	final WorkSurfaceCraftingManager workSurface;

	public WorkSurfaceRecipeSorter(
			WorkSurfaceCraftingManager workSurfaceCraftingManager) {
		this.workSurface = workSurfaceCraftingManager;
	}

	public int compareRecipes(IRecipe iRecipe1, IRecipe iRecipe2) {
		return iRecipe1 instanceof WorkSurfaceShaplessRecipes
				&& iRecipe2 instanceof WorkSurfaceShapedRecipes ? 1
				: (iRecipe2 instanceof WorkSurfaceShaplessRecipes
						&& iRecipe1 instanceof WorkSurfaceShapedRecipes ? -1
						: (iRecipe2.getRecipeSize() < iRecipe1.getRecipeSize() ? -1
								: (iRecipe2.getRecipeSize() > iRecipe1
										.getRecipeSize() ? 1 : 0)));
	}

	@Override
	public int compare(Object o1, Object o2) {
		return this.compareRecipes((IRecipe)o1, (IRecipe)o2);
	}

}
