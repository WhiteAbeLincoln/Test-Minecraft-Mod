package com.abe.tutorial.crafting;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFishFood;
import net.minecraft.item.ItemStack;

public class CustomFurnaceRecipes
{
    //private static final CustomFurnaceRecipes smeltingBase = new CustomFurnaceRecipes();
    /** The list of smelting results. */
    private Map smeltingList;
    private Map experienceList;
    private static final String __OBFID = "CL_00000085";

    /*/**
     * Used to call methods addSmelting and getSmeltingResult.
     */
    /*public static CustomFurnaceRecipes smelting()
    {
        return smeltingBase;
    }*/

    public CustomFurnaceRecipes(Map<ItemStack, ItemStack> smeltMap, Map xpMap)
    {
    	smeltingList = smeltMap;
    	experienceList = xpMap;
    	
        this.addSmeltingRecipe(Blocks.iron_ore, new ItemStack(Items.iron_ingot), 0.7F);
        this.addSmeltingRecipe(Blocks.gold_ore, new ItemStack(Items.gold_ingot), 1.0F);
        this.addSmeltingRecipe(Blocks.diamond_ore, new ItemStack(Items.diamond), 1.0F);
        this.addSmeltingRecipe(Blocks.sand, new ItemStack(Blocks.glass), 0.1F);
        this.addSmeltingRecipe(Items.porkchop, new ItemStack(Items.cooked_porkchop), 0.35F);
        this.addSmeltingRecipe(Items.beef, new ItemStack(Items.cooked_beef), 0.35F);
        this.addSmeltingRecipe(Items.chicken, new ItemStack(Items.cooked_chicken), 0.35F);
        this.addSmeltingRecipe(Blocks.cobblestone, new ItemStack(Blocks.stone), 0.1F);
        this.addSmeltingRecipe(Items.clay_ball, new ItemStack(Items.brick), 0.3F);
        this.addSmeltingRecipe(Blocks.clay, new ItemStack(Blocks.hardened_clay), 0.35F);
        this.addSmeltingRecipe(Blocks.cactus, new ItemStack(Items.dye, 1, 2), 0.2F);
        this.addSmeltingRecipe(Blocks.log, new ItemStack(Items.coal, 1, 1), 0.15F);
        this.addSmeltingRecipe(Blocks.log2, new ItemStack(Items.coal, 1, 1), 0.15F);
        this.addSmeltingRecipe(Blocks.emerald_ore, new ItemStack(Items.emerald), 1.0F);
        this.addSmeltingRecipe(Items.potato, new ItemStack(Items.baked_potato), 0.35F);
        this.addSmeltingRecipe(Blocks.netherrack, new ItemStack(Items.netherbrick), 0.1F);
        ItemFishFood.FishType[] afishtype = ItemFishFood.FishType.values();
        int i = afishtype.length;

        for (int j = 0; j < i; ++j)
        {
            ItemFishFood.FishType fishtype = afishtype[j];

            if (fishtype.func_150973_i())
            {
                this.addSmeltingRecipe(new ItemStack(Items.fish, 1, fishtype.func_150976_a()), new ItemStack(Items.cooked_fished, 1, fishtype.func_150976_a()), 0.35F);
            }
        }

        this.addSmeltingRecipe(Blocks.coal_ore, new ItemStack(Items.coal), 0.1F);
        this.addSmeltingRecipe(Blocks.redstone_ore, new ItemStack(Items.redstone), 0.7F);
        this.addSmeltingRecipe(Blocks.lapis_ore, new ItemStack(Items.dye, 1, 4), 0.2F);
        this.addSmeltingRecipe(Blocks.quartz_ore, new ItemStack(Items.quartz), 0.2F);
    }

    /*public CustomFurnaceRecipes() {
    	smeltingList = new HashMap();
    	experienceList = new HashMap();
	}*/

	/**
     * Adds a recipe to the HashMap
     * @param blockInput	the block that is smelted
     * @param itemStackOutput	the item stack that is returned
     * @param xp	the amount of xp returned
     */
    public void addSmeltingRecipe(Block blockInput, ItemStack itemStackOutput, float xp)
    {
        this.addSmeltingRecipe(Item.getItemFromBlock(blockInput), itemStackOutput, xp);
    }

    /**
     * Adds a recipe to the HashMap
     * @param itemInput	the item that is smelted
     * @param itemStackOutput	the item stack that is returned
     * @param xp	the amount of xp returned
     */
    public void addSmeltingRecipe(Item itemInput, ItemStack itemStackOutput, float xp)
    {
        this.addSmeltingRecipe(new ItemStack(itemInput, 1, 32767), itemStackOutput, xp);
    }

    /**
     * Adds a recipe to the HashMap
     * @param itemStackInput	the item stack that is smelted
     * @param itemStackOutput	the item stack that is returned
     * @param xp	the amount of xp returned
     */
    public void addSmeltingRecipe(ItemStack itemStackInput, ItemStack itemStackOutput, float xp)
    {
        this.smeltingList.put(itemStackInput, itemStackOutput);
        this.experienceList.put(itemStackOutput, Float.valueOf(xp));
    }

    /**
     * Returns the smelting result of an item.
     */
    public ItemStack getSmeltingResult(ItemStack itemStackInput)
    {
        Iterator iterator = this.smeltingList.entrySet().iterator();		//builds an iterator to look through the hashmap
        Entry entry;														//represents a particular key, value pair

        do										//parses through the list	
        {
            if (!iterator.hasNext())			//if there is nothing left in the list return null
            {
                return null;
            }

            entry = (Entry)iterator.next();		//stores element into the entry
        }
        while (!this.compareRecipe(itemStackInput, (ItemStack)entry.getKey()));		//compares the specified input to the stored recipe input

        return (ItemStack)entry.getValue();											//once correct is found, return the value (the recipe output)
    }

    private boolean compareRecipe(ItemStack itemStackInput, ItemStack itemStackKey)		//compares the two inputs
    {
        return itemStackKey.getItem() == itemStackInput.getItem() && (itemStackKey.getItemDamage() == 32767 || itemStackKey.getItemDamage() == itemStackInput.getItemDamage());
    }

    /**
     * Gets the list of smelting recipes
     * @return	the smelting HashMap
     */
    public Map getSmeltingList()
    {
        return this.smeltingList;
    }

	/**
	 * Gets the amount of xp that would be output when this item is smelt
	 * @param itemStackOutput	the item that would be received
	 * @return	a float representing the amount of xp
	 */
    public float getXpOutput(ItemStack itemStackOutput)
    {
        float ret = itemStackOutput.getItem().getSmeltingExperience(itemStackOutput);		//tries to get the experience from the item
        if (ret != -1) return ret;						//if the xp was good (was stored in item) return that value

        Iterator iterator = this.experienceList.entrySet().iterator();			//parses through map, just like getSmeltingResult
        Entry entry;

        do
        {
            if (!iterator.hasNext())
            {
                return 0.0F;
            }

            entry = (Entry)iterator.next();
        }
        while (!this.compareRecipe(itemStackOutput, (ItemStack)entry.getKey()));

        return ((Float)entry.getValue()).floatValue();
    }
}
