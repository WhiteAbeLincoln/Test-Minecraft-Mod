package com.abe.tutorial.crafting;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFishFood;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;

public class MashingRecipes{
	private static final MashingRecipes smeltingBase = new MashingRecipes();
    /** The list of smelting results. */
    private Map smeltingList = new HashMap();
    private Map experienceList = new HashMap();
    private static final String __OBFID = "CL_00000085";

    /**
     * Used to call methods addSmelting and getSmeltingResult.
     */
    public static MashingRecipes smelting()
    {
        return smeltingBase;
    }

    private MashingRecipes()
    {
       
    }

    public void addMashRecipe(Block block, Block block2, ItemStack itemStackOutput, float xp)
    {
        this.addMashRecipe(Item.getItemFromBlock(block), Item.getItemFromBlock(block2), itemStackOutput, xp);
    }

    public void addMashRecipe(Item item, Item item2, ItemStack itemStackOutput, float xp)
    {
        this.addMashRecipe(new ItemStack(item, 1, 32767), new ItemStack(item2, 1, 32767), itemStackOutput, xp);
    }

    public void addMashRecipe(ItemStack itemStack1, ItemStack itemStack2, ItemStack itemStackOutput, float xp)
    {
        this.smeltingList.put(new ItemStack[]{itemStack1,itemStack2}, itemStackOutput);
        this.experienceList.put(itemStackOutput, Float.valueOf(xp));
    }

    /**
     * Returns the mashing result of 2 items.
     */
    public ItemStack getSmeltingResult(ItemStack itemStack, ItemStack itemStack2)
    {
        Iterator iterator = this.smeltingList.entrySet().iterator();
        Entry entry;
        ItemStack[] arrayIStacks = new ItemStack[]{itemStack, itemStack2};

        do
        {
            if (!iterator.hasNext())
            {
                return null;
            }

            entry = (Entry)iterator.next();
        }
        while (!this.compareRecipe(arrayIStacks, (ItemStack[])entry.getKey()));

        return (ItemStack)entry.getValue();
    }

    private boolean compareRecipe(ItemStack[] itemStackArray, ItemStack[] storedIStacks)
    {
    	ItemStack itemStack0 = itemStackArray[0];
    	ItemStack itemStack1 = itemStackArray[1];
    	ItemStack storedStack0 = storedIStacks[0];
    	ItemStack storedStack1 = storedIStacks[1];
    	
        return (compareIStack(itemStack0, storedStack0) && compareIStack(itemStack1, storedStack1) || compareIStack(itemStack0, storedStack1) && compareIStack(itemStack1, storedStack0));
    }

    private boolean compareIStack(ItemStack itemStack, ItemStack storedStack){
    	return storedStack.getItem() == itemStack.getItem() && (storedStack.getItemDamage() == 32767 || storedStack.getItemDamage() == itemStack.getItemDamage());
    }
    
    public Map getSmeltingList()
    {
        return this.smeltingList;
    }

    public float getXpOutput(ItemStack itemStackOutput)
    {
        float ret = itemStackOutput.getItem().getSmeltingExperience(itemStackOutput);
        if (ret != -1) return ret;

        Iterator iterator = this.experienceList.entrySet().iterator();
        Entry entry;

        do
        {
            if (!iterator.hasNext())
            {
                return 0.0F;
            }

            entry = (Entry)iterator.next();
        }
        while (!this.compareIStack(itemStackOutput, (ItemStack)entry.getKey()));

        return ((Float)entry.getValue()).floatValue();
    }
	
}
