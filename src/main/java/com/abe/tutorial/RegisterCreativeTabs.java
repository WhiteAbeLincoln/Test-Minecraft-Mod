package com.abe.tutorial;

import com.abe.tutorial.blocks.RegisterBlocks;
import com.abe.tutorial.items.RegisterItems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class RegisterCreativeTabs {

	public static CreativeTabs tutorialTabBlocks;
	public static CreativeTabs tutorialTabItems;
	public static CreativeTabs tutorialTabTools;
	public static CreativeTabs tutorialTabCombat;
	public static CreativeTabs tutorialTabMachines;
	
	public static void init(){
		//creates a new creative tab -- pretty simple, Name is passed as a parameter, override the getTabIconItem, returning an item with the desired Icon
		//call from the PreInit !!Before you load the items and blocks!!
		tutorialTabBlocks = new CreativeTabs("TutorialBlocks") {
			@SideOnly(Side.CLIENT)
			@Override
			public Item getTabIconItem() {
				return Item.getItemFromBlock(RegisterBlocks.oreUnstableOre);
			}
		};
		tutorialTabItems = new CreativeTabs("TutorialItems") {
			@SideOnly(Side.CLIENT)
			@Override
			public Item getTabIconItem() {
				return RegisterItems.itemUnstableIngot;
			}
		};
		tutorialTabTools = new CreativeTabs("TutorialTools") {
			@SideOnly(Side.CLIENT)
			@Override
			public Item getTabIconItem() {
				return RegisterItems.itemIronHammer;
			}
		};
		tutorialTabCombat = new CreativeTabs("TutorialCombat") {
			@SideOnly(Side.CLIENT)
			@Override
			public Item getTabIconItem() {
				return RegisterItems.itemUnstableSwordEnhanced;
			}
		};
		tutorialTabMachines = new CreativeTabs("TutorialMachines") {
			@SideOnly(Side.CLIENT)
			@Override
			public Item getTabIconItem() {
				return Item.getItemFromBlock(RegisterBlocks.blockAlabasterOvenIdle);
			}
		};
	}
}
